# Tor specification repository

**IF YOU WANT TO READ THESE SPECS,
[GO HERE](https://people.torproject.org/~nickm/volatile/mdbook-specs/index.html).**  (It is a temporoary location; we intend to move it to `spec.torproject.org`.

This is the central location for editing and maintaining the Tor
specifications and proposals for feature changes.

The specification is rendered at
[better-url-here](https://people.torproject.org/~nickm/volatile/mdbook-specs/index.html);
if you want to _read_ the specifications, that is the place to start.

The official site for this repository is on
[gitlab.torproject.org](https://gitlab.torproject.org/tpo/core/torspec/)

We use mdBook to convert these specifications into the webpages you see above.
For more information about editing them, start with the
[mdBook manual]([mdBook]((https://rust-lang.github.io/mdBook/)).

The core of this repository is:
 * `spec` — A set of markdown files which contain the specifications.
   The file `src/SUMAMRY.md` controls their ordering within the
   rendered specification.
 * `proposals` — A directory of change proposals for the Tor protocols.

Additionally, this repository contains:
 * `mdbook` — A configuration file controlling how the specifications
   are rendered.
 * `attic` — Obsolete specifications describing no-longer-in-use
   pieces of the Tor specification, and obsolete formats of the
   existing specifications.
