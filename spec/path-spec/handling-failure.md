<a id="path-spec.txt-2.5"></a>
## Handling failure

If an attempt to extend a circuit fails (either because the first create
failed or a subsequent extend failed) then the circuit is torn down and is
no longer pending.  (XXXX really?)  Requests that might have been
supported by the pending circuit thus become unsupported, and a new
circuit needs to be constructed.

If a stream "begin" attempt fails with an EXITPOLICY error, we
decide that the exit node's exit policy is not correctly advertised,
so we treat the exit node as if it were a non-exit until we retrieve
a fresh descriptor for it.

Excessive amounts of either type of failure can indicate an
attack on anonymity. See section 7 for how excessive failure is handled.

