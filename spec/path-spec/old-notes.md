<a id="path-spec.txt-X"></a>
# Old notes

<a id="path-spec.txt-X.1"></a>
## Do we actually do this?

```text
How to deal with network down.
  - While all helpers are down/unreachable and there are no established
    or on-the-way testing circuits, launch a testing circuit. (Do this
    periodically in the same way we try to establish normal circuits
    when things are working normally.)
    (Testing circuits are a special type of circuit, that streams won't
    attach to by accident.)
  - When a testing circuit succeeds, mark all helpers up and hold
    the testing circuit open.
  - If a connection to a helper succeeds, close all testing circuits.
    Else mark that helper down and try another.
  - If the last helper is marked down and we already have a testing
    circuit established, then add the first hop of that testing circuit
    to the end of our helper node list, close that testing circuit,
    and go back to square one. (Actually, rather than closing the
    testing circuit, can we get away with converting it to a normal
    circuit and beginning to use it immediately?)
```

[Do we actually do any of the above?  If so, let's spec it.  If not, let's
remove it. -NM]

<a id="path-spec.txt-X.2"></a>
## A thing we could do to deal with reachability.

And as a bonus, it leads to an answer to Nick's attack ("If I pick
my helper nodes all on 18.0.0.0:*, then I move, you'll know where I
bootstrapped") -- the answer is to pick your original three helper nodes
without regard for reachability. Then the above algorithm will add some
more that are reachable for you, and if you move somewhere, it's more
likely (though not certain) that some of the originals will become useful.
Is that smart or just complex?

<a id="path-spec.txt-X.3"></a>
## Some stuff that worries me about entry guards. 2006 Jun, Nickm.

It is unlikely for two users to have the same set of entry guards.
Observing a user is sufficient to learn its entry guards.  So, as we move
around, entry guards make us linkable.  If we want to change guards when
our location (IP? subnet?) changes, we have two bad options.  We could

```text
    - Drop the old guards.  But if we go back to our old location,
      we'll not use our old guards.  For a laptop that sometimes gets used
      from work and sometimes from home, this is pretty fatal.
    - Remember the old guards as associated with the old location, and use
      them again if we ever go back to the old location.  This would be
      nasty, since it would force us to record where we've been.
```

[Do we do any of this now? If not, this should move into 099-misc or
098-todo. -NM]

