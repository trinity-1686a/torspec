<a id="path-spec.txt-1"></a>
# General operation

Tor begins building circuits as soon as it has enough directory
information to do so (see section 5 of dir-spec.txt).  Some circuits are
built preemptively because we expect to need them later (for user
traffic), and some are built because of immediate need (for user traffic
that no current circuit can handle, for testing the network or our
reachability, and so on).

```text
  [Newer versions of Tor (0.2.6.2-alpha and later):
   If the consensus contains Exits (the typical case), Tor will build both
   exit and internal circuits. When bootstrap completes, Tor will be ready
   to handle an application requesting an exit circuit to services like the
   World Wide Web.
```

If the consensus does not contain Exits, Tor will only build internal
circuits. In this case, earlier statuses will have included "internal"
as indicated above. When bootstrap completes, Tor will be ready to handle
an application requesting an internal circuit to hidden services at
".onion" addresses.

If a future consensus contains Exits, exit circuits may become available.]

When a client application creates a new stream (by opening a SOCKS
connection or launching a resolve request), we attach it to an appropriate
open circuit if one exists, or wait if an appropriate circuit is
in-progress. We launch a new circuit only
if no current circuit can handle the request.  We rotate circuits over
time to avoid some profiling attacks.

To build a circuit, we choose all the nodes we want to use, and then
construct the circuit.  Sometimes, when we want a circuit that ends at a
given hop, and we have an appropriate unused circuit, we "cannibalize" the
existing circuit and extend it to the new terminus.

These processes are described in more detail below.

This document describes Tor's automatic path selection logic only; path
selection can be overridden by a controller (with the EXTENDCIRCUIT and
ATTACHSTREAM commands).  Paths constructed through these means may
violate some constraints given below.

<a id="path-spec.txt-1.1"></a>
## Terminology

A "path" is an ordered sequence of nodes, not yet built as a circuit.

A "clean" circuit is one that has not yet been used for any traffic.

A "fast" or "stable" or "valid" node is one that has the 'Fast' or
'Stable' or 'Valid' flag
set respectively, based on our current directory information.  A "fast"
or "stable" circuit is one consisting only of "fast" or "stable" nodes.

In an "exit" circuit, the final node is chosen based on waiting stream
requests if any, and in any case it avoids nodes with exit policy of
"reject *:*". An "internal" circuit, on the other hand, is one where
the final node is chosen just like a middle node (ignoring its exit
policy).

A "request" is a client-side stream or DNS resolve that needs to be
served by a circuit.

A "pending" circuit is one that we have started to build, but which has
not yet completed.

A circuit or path "supports" a request if it is okay to use the
circuit/path to fulfill the request, according to the rules given below.
A circuit or path "might support" a request if some aspect of the request
is unknown (usually its target IP), but we believe the path probably
supports the request according to the rules given below.

<a id="path-spec.txt-1.2"></a>
## A relay's bandwidth

Old versions of Tor did not report bandwidths in network status
documents, so clients had to learn them from the routers' advertised
relay descriptors.

For versions of Tor prior to 0.2.1.17-rc, everywhere below where we
refer to a relay's "bandwidth", we mean its clipped advertised
bandwidth, computed by taking the smaller of the 'rate' and
'observed' arguments to the "bandwidth" element in the relay's
descriptor.  If a router's advertised bandwidth is greater than
MAX_BELIEVABLE_BANDWIDTH (currently 10 MB/s), we clipped to that
value.

For more recent versions of Tor, we take the bandwidth value declared
in the consensus, and fall back to the clipped advertised bandwidth
only if the consensus does not have bandwidths listed.

