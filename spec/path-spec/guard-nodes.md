<a id="path-spec.txt-5"></a>
# Guard nodes

We use Guard nodes (also called "helper nodes" in the research
literature) to prevent certain profiling attacks. For an overview of
our Guard selection algorithm -- which has grown rather complex -- see
guard-spec.txt.

<a id="path-spec.txt-5.1"></a>
## How consensus bandwidth weights factor into entry guard selection

When weighting a list of routers for choosing an entry guard, the following
consensus parameters (from the "bandwidth-weights" line) apply:

```text
      Wgg - Weight for Guard-flagged nodes in the guard position
      Wgm - Weight for non-flagged nodes in the guard Position
      Wgd - Weight for Guard+Exit-flagged nodes in the guard Position
      Wgb - Weight for BEGIN_DIR-supporting Guard-flagged nodes
      Wmb - Weight for BEGIN_DIR-supporting non-flagged nodes
      Web - Weight for BEGIN_DIR-supporting Exit-flagged nodes
      Wdb - Weight for BEGIN_DIR-supporting Guard+Exit-flagged nodes
```

Please see "bandwidth-weights" in §3.4.1 of dir-spec.txt for more in depth
descriptions of these parameters.

If a router has been marked as both an entry guard and an exit, then we
prefer to use it more, with our preference for doing so (roughly) linearly
increasing w.r.t. the router's non-guard bandwidth and bandwidth weight
(calculated without taking the guard flag into account).  From proposal
#236:

|
| Let Wpf denote the weight from the 'bandwidth-weights' line a
| client would apply to N for position p if it had the guard
| flag, Wpn the weight if it did not have the guard flag, and B the
| measured bandwidth of N in the consensus.  Then instead of choosing
| N for position p proportionally to Wpf*B or Wpn*B, clients should
| choose N proportionally to F*Wpf*B + (1-F)*Wpn*B.

where F is the weight as calculated using the above parameters.

