<a id="dir-spec.txt-D"></a>
# Inferring missing proto lines.

The directory authorities no longer allow versions of Tor before
0.2.4.18-rc.  But right now, there is no version of Tor in the consensus
before 0.2.4.19.  Therefore, we should disallow versions of Tor earlier
than 0.2.4.19, so that we can have the protocol list for all current Tor
versions include:

Cons=1-2 Desc=1-2 DirCache=1 HSDir=1 HSIntro=3 HSRend=1-2 Link=1-4
LinkAuth=1 Microdesc=1-2 Relay=1-2

For Desc, Microdesc and Cons, Tor versions before 0.2.7.stable should be
taken to only support version 1.

