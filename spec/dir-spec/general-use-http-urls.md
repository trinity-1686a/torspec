<a id="dir-spec.txt-B"></a>
# General-use HTTP URLs

"Fingerprints" in these URLs are base16-encoded SHA1 hashes.

The most recent v3 consensus should be available at:

http://<hostname>/tor/status-vote/current/consensus.z

Similarly, the v3 microdescriptor consensus should be available at:

http://<hostname>/tor/status-vote/current/consensus-microdesc.z

Starting with Tor version 0.2.1.1-alpha is also available at:

http://<hostname>/tor/status-vote/current/consensus/<F1>+<F2>+<F3>.z

(NOTE: Due to squid proxy url limitations at most 96 fingerprints can be
retrieved in a single request.)

Where F1, F2, etc. are authority identity fingerprints the client trusts.
Servers will only return a consensus if more than half of the requested
authorities have signed the document, otherwise a 404 error will be sent
back.  The fingerprints can be shortened to a length of any multiple of
two, using only the leftmost part of the encoded fingerprint.  Tor uses
3 bytes (6 hex characters) of the fingerprint.

Clients SHOULD sort the fingerprints in ascending order.  Server MUST
accept any order.

Clients SHOULD use this format when requesting consensus documents from
directory authority servers and from caches running a version of Tor
that is known to support this URL format.

A concatenated set of all the current key certificates should be available
at:

http://<hostname>/tor/keys/all.z

The key certificate for this server should be available at:

http://<hostname>/tor/keys/authority.z

The key certificate for an authority whose authority identity fingerprint
is <F> should be available at:

http://<hostname>/tor/keys/fp/<F>.z

The key certificate whose signing key fingerprint is <F> should be
available at:

http://<hostname>/tor/keys/sk/<F>.z

The key certificate whose identity key fingerprint is <F> and whose signing
key fingerprint is <S> should be available at:

http://<hostname>/tor/keys/fp-sk/<F>-<S>.z

(As usual, clients may request multiple certificates using:

http://<hostname>/tor/keys/fp-sk/<F1>-<S1>+<F2>-<S2>.z  )

[The above fp-sk format was not supported before Tor 0.2.1.9-alpha.]

The most recent descriptor for a server whose identity key has a
fingerprint of <F> should be available at:

http://<hostname>/tor/server/fp/<F>.z

The most recent descriptors for servers with identity fingerprints
<F1>,<F2>,<F3> should be available at:

http://<hostname>/tor/server/fp/<F1>+<F2>+<F3>.z

(NOTE: Due to squid proxy url limitations at most 96 fingerprints can be
retrieved in a single request.

Implementations SHOULD NOT download descriptors by identity key
fingerprint. This allows a corrupted server (in collusion with a cache) to
provide a unique descriptor to a client, and thereby partition that client
from the rest of the network.)

The server descriptor with (descriptor) digest <D> (in hex) should be
available at:

http://<hostname>/tor/server/d/<D>.z

The most recent descriptors with digests <D1>,<D2>,<D3> should be
available at:

http://<hostname>/tor/server/d/<D1>+<D2>+<D3>.z

The most recent descriptor for this server should be at:

http://<hostname>/tor/server/authority.z

This is used for authorities, and also if a server is configured
as a bridge.  The official Tor implementations (starting at
0.1.1.x) use this resource to test whether a server's own DirPort
is reachable.  It is also useful for debugging purposes. 

A concatenated set of the most recent descriptors for all known servers
should be available at:

http://<hostname>/tor/server/all.z

Extra-info documents are available at the URLS

```text
      http://<hostname>/tor/extra/d/...
      http://<hostname>/tor/extra/fp/...
      http://<hostname>/tor/extra/all[.z]
      http://<hostname>/tor/extra/authority[.z]
         (As for /tor/server/ URLs: supports fetching extra-info
         documents by their digest, by the fingerprint of their servers,
         or all at once. When serving by fingerprint, we serve the
         extra-info that corresponds to the descriptor we would serve by
         that fingerprint. Only directory authorities of version
         0.2.0.1-alpha or later are guaranteed to support the first
         three classes of URLs.  Caches may support them, and MUST
         support them if they have advertised "caches-extra-info".)
```

For debugging, directories SHOULD expose non-compressed objects at
URLs like the above, but without the final ".z".  If the client uses
Accept-Encodings header, it should override the presence or absence
of the ".z" (see section 6.1).

Clients SHOULD use upper case letters (A-F) when base16-encoding
fingerprints.  Servers MUST accept both upper and lower case fingerprints
in requests.

