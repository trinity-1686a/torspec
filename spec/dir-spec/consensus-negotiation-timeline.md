<a id="dir-spec.txt-A"></a>
# Consensus-negotiation timeline.

```text
   Period begins: this is the Published time.
     Everybody sends votes
   Reconciliation: everybody tries to fetch missing votes.
     consensus may exist at this point.
   End of voting period:
     everyone swaps signatures.
   Now it's okay for caches to download
     Now it's okay for clients to download.

   Valid-after/valid-until switchover
```

