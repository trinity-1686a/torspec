<a id="dir-spec.txt-2.1"></a>
## Uploading server descriptors and extra-info documents

ORs SHOULD generate a new server descriptor and a new extra-info
document whenever any of the following events have occurred:

```text
      - A period of time (18 hrs by default) has passed since the last
        time a descriptor was generated.

      - A descriptor field other than bandwidth or uptime has changed.

      - Its uptime is less than 24h and bandwidth has changed by a factor of 2
        from the last time a descriptor was generated, and at least a given
        interval of time (3 hours by default) has passed since then.

      - Its uptime has been reset (by restarting).

      - It receives a networkstatus consensus in which it is not listed.

      - It receives a networkstatus consensus in which it is listed
        with the StaleDesc flag.

      [XXX this list is incomplete; see router_differences_are_cosmetic()
       in routerlist.c for others]
```

ORs SHOULD NOT publish a new server descriptor or extra-info document
if none of the above events have occurred and not much time has passed
(12 hours by default).

Tor versions older than 0.3.5.1-alpha ignore uptime when checking for
bandwidth changes.

After generating a descriptor, ORs upload them to every directory
authority they know, by posting them (in order) to the URL

http://<hostname:port>/tor/

Server descriptors may not exceed 20,000 bytes in length; extra-info
documents may not exceed 50,000 bytes in length. If they do, the
authorities SHOULD reject them.

