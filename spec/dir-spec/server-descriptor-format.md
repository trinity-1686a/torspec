<a id="dir-spec.txt-2.1.1"></a>
### Server descriptor format

Server descriptors consist of the following items.

In lines that take multiple arguments, extra arguments SHOULD be
accepted and ignored.  Many of the nonterminals below are defined in
section 2.1.3.

Note that many versions of Tor will generate an extra newline at the
end of their descriptors.  Implementations MUST tolerate one or
more blank lines at the end of a single descriptor or a list of
concatenated descriptors.  New implementations SHOULD NOT generate
such blank lines.

"router" nickname address ORPort SOCKSPort DirPort NL

[At start, exactly once.]

Indicates the beginning of a server descriptor.  "nickname" must be a
valid router nickname as specified in section 2.1.3.  "address" must
be an IPv4
address in dotted-quad format.  The last three numbers indicate the
TCP ports at which this OR exposes functionality. ORPort is a port at
which this OR accepts TLS connections for the main OR protocol;
SOCKSPort is deprecated and should always be 0; and DirPort is the
port at which this OR accepts directory-related HTTP connections.  If
any port is not supported, the value 0 is given instead of a port
number.  (At least one of DirPort and ORPort SHOULD be set;
authorities MAY reject any descriptor with both DirPort and ORPort of
0.)

```text
    "identity-ed25519" NL "-----BEGIN ED25519 CERT-----" NL certificate
           "-----END ED25519 CERT-----" NL
```

[Exactly once, in second position in document.]
[No extra arguments]

The certificate is a base64-encoded Ed25519 certificate (see
cert-spec.txt) with terminating =s removed.  When this element
is present, it MUST appear as the first or second element in
the router descriptor.

The certificate has CERT_TYPE of [04].  It must include a
signed-with-ed25519-key extension (see cert-spec.txt,
section 2.2.1), so that we can extract the master identity key.

[Before Tor 0.4.5.1-alpha, this field was optional.]

"master-key-ed25519" SP MasterKey NL

[Exactly once]

Contains the base-64 encoded ed25519 master key as a single
argument.  If it is present, it MUST match the identity key
in the identity-ed25519 entry.

[Before Tor 0.4.5.1-alpha, this field was optional.]

"bandwidth" bandwidth-avg bandwidth-burst bandwidth-observed NL

[Exactly once]

Estimated bandwidth for this router, in bytes per second.  The
"average" bandwidth is the volume per second that the OR is willing to
sustain over long periods; the "burst" bandwidth is the volume that
the OR is willing to sustain in very short intervals.  The "observed"
value is an estimate of the capacity this relay can handle.  The
relay remembers the max bandwidth sustained output over any ten
second period in the past 5 days, and another sustained input.  The
"observed" value is the lesser of these two numbers.

Tor versions released before 2018 only kept bandwidth-observed for one
day. These versions are no longer supported or recommended.

"platform" string NL

[At most once]

A human-readable string describing the system on which this OR is
running.  This MAY include the operating system, and SHOULD include
the name and version of the software implementing the Tor protocol.

"published" YYYY-MM-DD HH:MM:SS NL

[Exactly once]

The time, in UTC, when this descriptor (and its corresponding
extra-info document if any) was generated.

"fingerprint" fingerprint NL

[At most once]

A fingerprint (a HASH_LEN-byte of asn1 encoded public key, encoded in
hex, with a single space after every 4 characters) for this router's
identity key. A descriptor is considered invalid (and MUST be
rejected) if the fingerprint line does not match the public key.

```text
       [We didn't start parsing this line until Tor 0.1.0.6-rc; it should
        be marked with "opt" until earlier versions of Tor are obsolete.]

    "hibernating" bool NL

       [At most once]
```

If the value is 1, then the Tor relay was hibernating when the
descriptor was published, and shouldn't be used to build circuits.

```text
       [We didn't start parsing this line until Tor 0.1.0.6-rc; it should be
        marked with "opt" until earlier versions of Tor are obsolete.]

    "uptime" number NL

       [At most once]

       The number of seconds that this OR process has been running.

    "onion-key" NL a public key in PEM format
```

[Exactly once]
[No extra arguments]

This key is used to encrypt CREATE cells for this OR.  The key MUST be
accepted for at least 1 week after any new key is published in a
subsequent descriptor. It MUST be 1024 bits.

The key encoding is the encoding of the key as a PKCS#1 RSAPublicKey
structure, encoded in base64, and wrapped in "-----BEGIN RSA PUBLIC
KEY-----" and "-----END RSA PUBLIC KEY-----".

"onion-key-crosscert" NL a RSA signature in PEM format.

[Exactly once]
[No extra arguments]

This element contains an RSA signature, generated using the
onion-key, of the following:

```text
          A SHA1 hash of the RSA identity key,
            i.e. RSA key from "signing-key" (see below) [20 bytes]
          The Ed25519 identity key,
            i.e. Ed25519 key from "master-key-ed25519" [32 bytes]
```

If there is no Ed25519 identity key, or if in some future version
there is no RSA identity key, the corresponding field must be
zero-filled.

Parties verifying this signature MUST allow additional data
beyond the 52 bytes listed above.

This signature proves that the party creating the descriptor
had control over the secret key corresponding to the
onion-key.

[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.]

"ntor-onion-key" base-64-encoded-key

[Exactly once]

A curve25519 public key used for the ntor circuit extended
handshake.  It's the standard encoding of the OR's curve25519
public key, encoded in base 64.  The trailing '=' sign MAY be
omitted from the base64 encoding.  The key MUST be accepted
for at least 1 week after any new key is published in a
subsequent descriptor.

[Before Tor 0.4.5.1-alpha, this field was optional.]

```text
    "ntor-onion-key-crosscert" SP Bit NL
           "-----BEGIN ED25519 CERT-----" NL certificate
           "-----END ED25519 CERT-----" NL
```

[Exactly once]
[No extra arguments]

A signature created with the ntor-onion-key, using the
certificate format documented in cert-spec.txt, with type
[0a].  The signed key here is the master identity key.

Bit must be "0" or "1".  It indicates the sign of the ed25519
public key corresponding to the ntor onion key.  If Bit is "0",
then implementations MUST guarantee that the x-coordinate of
the resulting ed25519 public key is positive.  Otherwise, if
Bit is "1", then the sign of the x-coordinate MUST be negative.

To compute the ed25519 public key corresponding to a curve25519
key, and for further explanation on key formats, see appendix C.

This signature proves that the party creating the descriptor
had control over the secret key corresponding to the
ntor-onion-key.

[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.]

"signing-key" NL a public key in PEM format

[Exactly once]
[No extra arguments]

The OR's long-term RSA identity key.  It MUST be 1024 bits.

The encoding is as for "onion-key" above.

"accept" exitpattern NL
"reject" exitpattern NL

[Any number]

These lines describe an "exit policy": the rules that an OR follows
when deciding whether to allow a new stream to a given address.  The
'exitpattern' syntax is described below.  There MUST be at least one
such entry.  The rules are considered in order; if no rule matches,
the address will be accepted.  For clarity, the last such entry SHOULD
be accept *:* or reject *:*.

"ipv6-policy" SP ("accept" / "reject") SP PortList NL

[At most once.]

An exit-policy summary as specified in sections 3.4.1 and 3.8.2,
summarizing
the router's rules for connecting to IPv6 addresses. A missing
"ipv6-policy" line is equivalent to "ipv6-policy reject
1-65535".

"overload-general" SP version SP YYYY-MM-DD HH:MM:SS NL

[At most once.]

Indicates that a relay has reached an "overloaded state" which can be
one or many of the following load metrics:

```text
         - Any OOM invocation due to memory pressure
         - Any ntor onionskins are dropped
         - TCP port exhaustion
```

The timestamp is when at least one metrics was detected. It should always
be at the hour and thus, as an example, "2020-01-10 13:00:00" is an
expected timestamp. Because this is a binary state, if the line is
present, we consider that it was hit at the very least once somewhere
between the provided timestamp and the "published" timestamp of the
document which is when the document was generated.

The overload-general line should remain in place for 72 hours since last
triggered.  If the limits are reached again in this period, the timestamp
is updated, and this 72 hour period restarts.

The 'version' field is set to '1' for now.

```text
      (Introduced in tor-0.4.6.1-alpha, but moved from extra-info to general
       descriptor in tor-0.4.6.2-alpha)

    "router-sig-ed25519" SP Signature NL

       [Exactly once.]
```

It MUST be the next-to-last element in the descriptor, appearing
immediately before the RSA signature. It MUST contain an Ed25519
signature of a SHA256 digest of the entire document. This digest is
taken from the first character up to and including the first space
after the "router-sig-ed25519" string. Before computing the digest,
the string "Tor router descriptor signature v1" is prefixed to the
document.

The signature is encoded in Base64, with terminating =s removed.

The signing key in the identity-ed25519 certificate MUST
be the one used to sign the document.

[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.]

"router-signature" NL Signature NL

[At end, exactly once]
[No extra arguments]

The "SIGNATURE" object contains a signature of the PKCS1-padded
hash of the entire server descriptor, taken from the beginning of the
"router" line, through the newline after the "router-signature" line.
The server descriptor is invalid unless the signature is performed
with the router's identity key.

"contact" info NL

[At most once]

Describes a way to contact the relay's administrator, preferably
including an email address and a PGP key fingerprint.

"bridge-distribution-request" SP Method NL

[At most once, bridges only.]

The "Method" describes how a Bridge address is distributed by
BridgeDB. Recognized methods are: "none", "any", "https", "email",
"moat". If set to "none", BridgeDB will avoid distributing your bridge
address. If set to "any", BridgeDB will choose how to distribute your
bridge address. Choosing any of the other methods will tell BridgeDB to
distribute your bridge via a specific method:

```text
          - "https" specifies distribution via the web interface at
             https://bridges.torproject.org;
          - "email" specifies distribution via the email autoresponder at
            bridges@torproject.org;
          - "moat" specifies distribution via an interactive menu inside Tor
            Browser; and

        Potential future "Method" specifiers must be as follows:
            Method = (KeywordChar | "_") +
```

All bridges SHOULD include this line. Non-bridges MUST NOT include
it.

BridgeDB SHOULD treat unrecognized Method values as if they were
"none".

(Default: "any")

[This line was introduced in 0.3.2.3-alpha, with a minimal backport
to 0.2.5.16, 0.2.8.17, 0.2.9.14, 0.3.0.13, 0.3.1.9, and later.]

"family" names NL

[At most once]

'Names' is a space-separated list of relay nicknames or
hexdigests. If two ORs list one another in their "family" entries,
then OPs should treat them as a single OR for the purpose of path
selection.

For example, if node A's descriptor contains "family B", and node B's
descriptor contains "family A", then node A and node B should never
be used on the same circuit.

```text
    "read-history" YYYY-MM-DD HH:MM:SS (NSEC s) NUM,NUM,NUM,NUM,NUM... NL
        [At most once]
    "write-history" YYYY-MM-DD HH:MM:SS (NSEC s) NUM,NUM,NUM,NUM,NUM... NL
        [At most once]
```

(These fields once appeared in router descriptors, but have
appeared in extra-info descriptors since 0.2.0.x.)

"eventdns" bool NL

[At most once]

Declare whether this version of Tor is using the newer enhanced
dns logic.  Versions of Tor with this field set to false SHOULD NOT
be used for reverse hostname lookups.

```text
        [This option is obsolete.  All Tor current relays should be presumed
         to have the evdns backend.]

   "caches-extra-info" NL
```

[At most once.]
[No extra arguments]

Present only if this router is a directory cache that provides
extra-info documents.

[Versions before 0.2.0.1-alpha don't recognize this]

"extra-info-digest" SP sha1-digest [SP sha256-digest] NL

[At most once]

"sha1-digest" is a hex-encoded SHA1 digest (using upper-case characters)
of the router's extra-info document, as signed in the router's
extra-info (that is, not including the signature).  (If this field is
absent, the router is not uploading a corresponding extra-info
document.)

"sha256-digest" is a base64-encoded SHA256 digest of the extra-info
document. Unlike the "sha1-digest", this digest is calculated over the
entire document, including the signature. This difference is due to
a long-lived bug in the tor implementation that it would be difficult
to roll out an incremental fix for, not a design choice. Future digest
algorithms specified should not include the signature in the data used
to compute the digest.

[Versions before 0.2.7.2-alpha did not include a SHA256 digest.]
[Versions before 0.2.0.1-alpha don't recognize this field at all.]

"hidden-service-dir" NL

[At most once.]

Present only if this router stores and serves hidden service
descriptors. This router supports the descriptor versions declared
in the HSDir "proto" entry. If there is no "proto" entry, this
router supports version 2 descriptors.

```text
   "protocols" SP "Link" SP LINK-VERSION-LIST SP "Circuit" SP
          CIRCUIT-VERSION-LIST NL

       [At most once.]
```

An obsolete list of protocol versions, superseded by the "proto"
entry.  This list was never parsed, and has not been emitted
since Tor 0.2.9.4-alpha. New code should neither generate nor
parse this line.

"allow-single-hop-exits" NL

[At most once.]
[No extra arguments]

Present only if the router allows single-hop circuits to make exit
connections.  Most Tor relays do not support this: this is
included for specialized controllers designed to support perspective
access and such. This is obsolete in tor version >= 0.3.1.0-alpha.

"or-address" SP ADDRESS ":" PORT NL

[Any number]

ADDRESS = IP6ADDR | IP4ADDR
IPV6ADDR = an ipv6 address, surrounded by square brackets.
IPV4ADDR = an ipv4 address, represented as a dotted quad.
PORT = a number between 1 and 65535 inclusive.

An alternative for the address and ORPort of the "router" line, but with
two added capabilities:

```text
         * or-address can be either an IPv4 or IPv6 address
         * or-address allows for multiple ORPorts and addresses
```

A descriptor SHOULD NOT include an or-address line that does nothing but
duplicate the address:port pair from its "router" line.

The ordering of or-address lines and their PORT entries matter because
Tor MAY accept a limited number of address/port pairs. As of
Tor 0.2.3.x only the first address/port pair is advertised and used.

"tunnelled-dir-server" NL

[At most once.]
[No extra arguments]

```text
       Present if the router accepts "tunneled" directory requests using a
       BEGIN_DIR cell over the router's OR port.
          (Added in 0.2.8.1-alpha. Before this, Tor relays accepted
          tunneled directory requests only if they had a DirPort open,
          or if they were bridges.)

   "proto" SP Entries NL

       [Exactly once.]
```

Entries =
Entries = Entry
Entries = Entry SP Entries

Entry = Keyword "=" Values

Values =
Values = Value
Values = Value "," Values

Value = Int
Value = Int "-" Int

Int = NON_ZERO_DIGIT
Int = Int DIGIT

Each 'Entry' in the "proto" line indicates that the Tor relay supports
one or more versions of the protocol in question.  Entries should be
sorted by keyword.  Values should be numerically ascending within each
entry.  (This implies that there should be no overlapping ranges.)
Ranges should be represented as compactly as possible. Ints must be no
larger than 63.

This field was first added in Tor 0.2.9.x.

[Before Tor 0.4.5.1-alpha, this field was optional.]

