<a id="dir-spec.txt-3"></a>
# Directory authority operation and formats

Every authority has two keys used in this protocol: a signing key, and
an authority identity key.  (Authorities also have a router identity
key used in their role as a router and by earlier versions of the
directory protocol.)  The identity key is used from time to time to
sign new key certificates using new signing keys; it is very sensitive.
The signing key is used to sign key certificates and status documents.

