<a id="dir-spec.txt-3.2"></a>
## Accepting server descriptor and extra-info document uploads

When a router posts a signed descriptor to a directory authority, the
authority first checks whether it is well-formed and correctly
self-signed.  If it is, the authority next verifies that the nickname
in question is not already assigned to a router with a different
public key.
Finally, the authority MAY check that the router is not blacklisted
because of its key, IP, or another reason.

An authority also keeps a record of all the Ed25519/RSA1024
identity key pairs that it has seen before.  It rejects any
descriptor that has a known Ed/RSA identity key that it has
already seen accompanied by a different RSA/Ed identity key
in an older descriptor.

At a future date, authorities will begin rejecting all
descriptors whose RSA key was previously accompanied by an
Ed25519 key, if the descriptor does not list an Ed25519 key.

At a future date, authorities will begin rejecting all descriptors
that do not list an Ed25519 key.

If the descriptor passes these tests, and the authority does not already
have a descriptor for a router with this public key, it accepts the
descriptor and remembers it.

If the authority _does_ have a descriptor with the same public key, the
newly uploaded descriptor is remembered if its publication time is more
recent than the most recent old descriptor for that router, and either:

```text
      - There are non-cosmetic differences between the old descriptor and the
        new one.
      - Enough time has passed between the descriptors' publication times.
        (Currently, 2 hours.)
```

Differences between server descriptors are "non-cosmetic" if they would be
sufficient to force an upload as described in section 2.1 above.

Note that the "cosmetic difference" test only applies to uploaded
descriptors, not to descriptors that the authority downloads from other
authorities.

When a router posts a signed extra-info document to a directory authority,
the authority again checks it for well-formedness and correct signature,
and checks that its matches the extra-info-digest in some router
descriptor that it believes is currently useful.  If so, it accepts it and
stores it and serves it as requested.  If not, it drops it.

