<a id="dir-spec.txt-6"></a>
# Standards compliance

All clients and servers MUST support HTTP 1.0.  Clients and servers MAY
support later versions of HTTP as well.

<a id="dir-spec.txt-6.1"></a>
## HTTP headers

Servers SHOULD set Content-Encoding to the algorithm used to compress the
document(s) being served.  Recognized algorithms are:

```text
     - "identity"     -- RFC2616 section 3.5
     - "deflate"      -- RFC2616 section 3.5
     - "gzip"         -- RFC2616 section 3.5
     - "x-zstd"       -- The zstandard compression algorithm (www.zstd.net)
     - "x-tor-lzma"   -- The lzma compression algorithm, with a "preset"
                         value no higher than 6.
```

Clients SHOULD use Accept-Encoding on most directory requests to indicate
which of the above compression algorithms they support.  If they omit it
(as Tor clients did before 0.3.1.1-alpha), then the server should serve
only "deflate" or "identity" encoded documents, based on the presence or
absence of the ".z" suffix on the requested URL.

Note that for anonymous directory requests (that is, requests made over
multi-hop circuits, like those for onion service lookups) implementations
SHOULD NOT advertise any Accept-Encoding values other than deflate.  To do
so would be to create a fingerprinting opportunity.

When receiving multiple documents, clients MUST accept compressed
concatenated documents and concatenated compressed documents as
equivalent.

Servers MAY set the Content-Length: header.  When they do, it should
match the number of compressed bytes that they are sending.

Servers MAY include an X-Your-Address-Is: header, whose value is the
apparent IP address of the client connecting to them (as a dotted quad).
For directory connections tunneled over a BEGIN_DIR stream, servers SHOULD
report the IP from which the circuit carrying the BEGIN_DIR stream reached
them.

Servers SHOULD disable caching of multiple network statuses or multiple
server descriptors.  Servers MAY enable caching of single descriptors,
single network statuses, the list of all server descriptors, a v1
directory, or a v1 running routers document.  XXX mention times.

<a id="dir-spec.txt-6.2"></a>
## HTTP status codes

Tor delivers the following status codes.  Some were chosen without much
thought; other code SHOULD NOT rely on specific status codes yet.

```text
  200 -- the operation completed successfully
      -- the user requested statuses or serverdescs, and none of the ones we
         requested were found (0.2.0.4-alpha and earlier).

  304 -- the client specified an if-modified-since time, and none of the
         requested resources have changed since that time.

  400 -- the request is malformed, or
      -- the URL is for a malformed variation of one of the URLs we support,
          or
      -- the client tried to post to a non-authority, or
      -- the authority rejected a malformed posted document, or

  404 -- the requested document was not found.
      -- the user requested statuses or serverdescs, and none of the ones
         requested were found (0.2.0.5-alpha and later).

  503 -- we are declining the request in order to save bandwidth
      -- user requested some items that we ordinarily generate or store,
         but we do not have any available.
```

