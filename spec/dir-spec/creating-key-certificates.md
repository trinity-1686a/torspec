<a id="dir-spec.txt-3.1"></a>
## Creating key certificates

Key certificates consist of the following items:

"dir-key-certificate-version" version NL

[At start, exactly once.]

Determines the version of the key certificate.  MUST be "3" for
the protocol described in this document.  Implementations MUST
reject formats they don't understand.

```text
    "dir-address" IPPort NL
        [At most once]

        An IP:Port for this authority's directory port.

    "fingerprint" fingerprint NL

        [Exactly once.]
```

Hexadecimal encoding without spaces based on the authority's
identity key.

"dir-identity-key" NL a public key in PEM format

[Exactly once.]
[No extra arguments]

The long-term authority identity key for this authority.  This key
SHOULD be at least 2048 bits long; it MUST NOT be shorter than
1024 bits.

"dir-key-published" YYYY-MM-DD HH:MM:SS NL

[Exactly once.]

The time (in UTC) when this document and corresponding key were
last generated.

Implementations SHOULD reject certificates that are published
too far in the future, though they MAY tolerate some clock skew.

"dir-key-expires" YYYY-MM-DD HH:MM:SS NL

[Exactly once.]

A time (in UTC) after which this key is no longer valid.

Implementations SHOULD reject expired certificates, though they
MAY tolerate some clock skew.

"dir-signing-key" NL a key in PEM format

[Exactly once.]
[No extra arguments]

The directory server's public signing key.  This key MUST be at
least 1024 bits, and MAY be longer.

"dir-key-crosscert" NL CrossSignature NL

[Exactly once.]
[No extra arguments]

CrossSignature is a signature, made using the certificate's signing
key, of the digest of the PKCS1-padded hash of the certificate's
identity key.  For backward compatibility with broken versions of the
parser, we wrap the base64-encoded signature in -----BEGIN ID
SIGNATURE---- and -----END ID SIGNATURE----- tags.  Implementations
MUST allow the "ID " portion to be omitted, however.

Implementations MUST verify that the signature is a correct signature
of the hash of the identity key using the signing key.

"dir-key-certification" NL Signature NL

[At end, exactly once.]
[No extra arguments]

A document signature as documented in section 1.3, using the
initial item "dir-key-certificate-version" and the final item
"dir-key-certification", signed with the authority identity key.

Authorities MUST generate a new signing key and corresponding
certificate before the key expires.

