<a id="dir-spec.txt-3.4.2"></a>
### Assigning flags in a vote

(This section describes how directory authorities choose which status
flags to apply to routers. Later directory authorities MAY do things
differently, so long as clients keep working well.  Clients MUST NOT
depend on the exact behaviors in this section.)

In the below definitions, a router is considered "active" if it is
running, valid, and not hibernating.

When we speak of a router's bandwidth in this section, we mean either
its measured bandwidth, or its advertised bandwidth. If a sufficient
threshold (configurable with MinMeasuredBWsForAuthToIgnoreAdvertised,
500 by default) of routers have measured bandwidth values, then the
authority bases flags on _measured_ bandwidths, and treats nodes with
non-measured bandwidths as if their bandwidths were zero. Otherwise,
it uses measured bandwidths for nodes that have them, and advertised
bandwidths for other nodes.

When computing thresholds based on percentiles of nodes, an authority
only considers nodes that are active, that have not been
omitted as a sybil (see below), and whose bandwidth is at least
4 KB.  Nodes that don't meet these criteria do not influence any
threshold calculations (including calculation of stability and uptime
and bandwidth thresholds) and also do not have their Exit status
change.

"Valid" -- a router is 'Valid' if it is running a version of Tor not
known to be broken, and the directory authority has not blacklisted
it as suspicious.

```text
   "Named" --
   "Unnamed" -- Directory authorities no longer assign these flags.
      They were once used to determine whether a relay's nickname was
      canonically linked to its public key.
```

"Running" -- A router is 'Running' if the authority managed to connect to
it successfully within the last 45 minutes on all its published ORPorts.
Authorities check reachability on:

```text
     * the IPv4 ORPort in the "r" line, and
     * the IPv6 ORPort considered for the "a" line, if:
       * the router advertises at least one IPv6 ORPort, and
       * AuthDirHasIPv6Connectivity 1 is set on the authority.
```

A minority of voting authorities that set AuthDirHasIPv6Connectivity will
drop unreachable IPv6 ORPorts from the full consensus. Consensus method 27
in 0.3.3.x puts IPv6 ORPorts in the microdesc consensus, so that
authorities can drop unreachable IPv6 ORPorts from all consensus flavors.
Consensus method 28 removes IPv6 ORPorts from microdescriptors.

"Stable" -- A router is 'Stable' if it is active, and either its Weighted
MTBF is at least the median for known active routers or its Weighted MTBF
corresponds to at least 7 days. Routers are never called Stable if they are
running a version of Tor known to drop circuits stupidly.  (0.1.1.10-alpha
through 0.1.1.16-rc are stupid this way.)

To calculate weighted MTBF, compute the weighted mean of the lengths
of all intervals when the router was observed to be up, weighting
intervals by $\alpha^n$, where $n$ is the amount of time that has
passed since the interval ended, and $\alpha$ is chosen so that
measurements over approximately one month old no longer influence the
weighted MTBF much.

[XXXX what happens when we have less than 4 days of MTBF info.]

"Exit" -- A router is called an 'Exit' iff it allows exits to at
least one /8 address space on each of ports 80 and 443. (Up until
Tor version 0.3.2, the flag was assigned if relays exit to at least
two of the ports 80, 443, and 6667.)

"Fast" -- A router is 'Fast' if it is active, and its bandwidth is either in
the top 7/8ths for known active routers or at least 100KB/s.

"Guard" -- A router is a possible Guard if all of the following apply:

```text
       - It is Fast,
       - It is Stable,
       - Its Weighted Fractional Uptime is at least the median for "familiar"
         active routers,
       - It is "familiar",
       - Its bandwidth is at least AuthDirGuardBWGuarantee (if set, 2 MB by
         default), OR its bandwidth is among the 25% fastest relays,
       - It qualifies for the V2Dir flag as described below (this
         constraint was added in 0.3.3.x, because in 0.3.0.x clients
         started avoiding guards that didn't also have the V2Dir flag).
```

To calculate weighted fractional uptime, compute the fraction
of time that the router is up in any given day, weighting so that
downtime and uptime in the past counts less.

A node is 'familiar' if 1/8 of all active nodes have appeared more
recently than it, OR it has been around for a few weeks.

"Authority" -- A router is called an 'Authority' if the authority
generating the network-status document believes it is an authority.

"V2Dir" -- A router supports the v2 directory protocol or higher if it has
an open directory port OR a tunnelled-dir-server line in its router
descriptor, and it is running a version of the directory
protocol that supports the functionality clients need.  (Currently, every
supported version of Tor supports the functionality that clients need,
but some relays might set "DirCache 0" or set really low rate limiting,
making them unqualified to be a directory mirror, i.e. they will omit
the tunnelled-dir-server line from their descriptor.)

"HSDir" -- A router is a v2 hidden service directory if it stores and
serves v2 hidden service descriptors, has the Stable and Fast flag, and the
authority believes that it's been up for at least 96 hours (or the current
value of MinUptimeHidServDirectoryV2).

"MiddleOnly" -- An authority should vote for this flag if it believes
that a relay is unsuitable for use except as a middle relay.  When
voting for this flag, the authority should also vote against "Exit",
"Guard", "HsDir", and "V2Dir".  When voting for this flag, if the
authority votes on the "BadExit" flag, the authority should vote in
favor of "BadExit".  (This flag was added in 0.4.7.2-alpha.)

"NoEdConsensus" -- authorities should not vote on this flag; it is
produced as part of the consensus for consensus method 22 or later.

"StaleDesc" -- authorities should vote to assign this flag if the
published time on the descriptor is over 18 hours in the past.  (This flag
was added in 0.4.0.1-alpha.)

"Sybil" -- authorities SHOULD NOT accept more than 2 relays on a single IP.
If this happens, the authority *should* vote for the excess relays, but
should omit the Running or Valid flags and instead should assign the "Sybil"
flag.  When there are more than 2 (or AuthDirMaxServersPerAddr) relays to
choose from, authorities should first prefer authorities to non-authorities,
then prefer Running to non-Running, and then prefer high-bandwidth to
low-bandwidth relays.  In this comparison, measured bandwidth is used unless
it is not present for a router, in which case advertised bandwidth is used.

Thus, the network-status vote includes all non-blacklisted,
non-expired, non-superseded descriptors.

The bandwidth in a "w" line should be taken as the best estimate
of the router's actual capacity that the authority has.  For now,
this should be the lesser of the observed bandwidth and bandwidth
rate limit from the server descriptor.  It is given in kilobytes
per second, and capped at some arbitrary value (currently 10 MB/s).

The Measured= keyword on a "w" line vote is currently computed
by multiplying the previous published consensus bandwidth by the
ratio of the measured average node stream capacity to the network
average. If 3 or more authorities provide a Measured= keyword for
a router, the authorities produce a consensus containing a "w"
Bandwidth= keyword equal to the median of the Measured= votes.

As a special case, if the "w" line in a vote is about a relay with the
Authority flag, it should not include a Measured= keyword. The goal is
to leave such relays marked as Unmeasured, so they can reserve their
attention for authority-specific activities. "w" lines for votes about
authorities may include the bandwidth authority's measurement using
a different keyword, e.g. MeasuredButAuthority=, so it can still be
reported and recorded for posterity.

The ports listed in a "p" line should be taken as those ports for
which the router's exit policy permits 'most' addresses, ignoring any
accept not for all addresses, ignoring all rejects for private
netblocks.  "Most" addresses are permitted if no more than 2^25
IPv4 addresses (two /8 networks) were blocked.  The list is encoded
as described in section 3.8.2.

