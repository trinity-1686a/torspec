<a id="pt-spec.txt-3.4"></a>
## Pluggable Transport Shutdown

The recommended way for Pluggable Transport using applications and
Pluggable Transports to handle graceful shutdown is as follows.

```text
     - (Parent) Set "TOR_PT_EXIT_ON_STDIN_CLOSE" (3.2.1) when
       launching the PT proxy, to indicate that stdin will be used
       for graceful shutdown notification.

     - (Parent) When the time comes to terminate the PT proxy:

       1. Close the PT proxy's stdin.
       2. Wait for a "reasonable" amount of time for the PT to exit.
       3. Attempt to use OS specific mechanisms to cause graceful
          PT shutdown (eg: 'SIGTERM')
       4. Use OS specific mechanisms to force terminate the PT
          (eg: 'SIGKILL', 'ProccessTerminate()').

     - PT proxies SHOULD monitor stdin, and exit gracefully when
       it is closed, if the parent supports that behavior.

     - PT proxies SHOULD handle OS specific mechanisms to gracefully
       terminate (eg: Install a signal handler on 'SIGTERM' that
       causes cleanup and a graceful shutdown if able).

     - PT proxies SHOULD attempt to detect when the parent has
       terminated (eg: via detecting that its parent process ID has
       changed on U*IX systems), and gracefully terminate.
```

