<a id="pt-spec.txt-B"></a>
# Appendix B: Example Server Pluggable Transport Session

Environment variables:

TOR_PT_MANAGED_TRANSPORT_VER=1
TOR_PT_STATE_LOCATION=/var/lib/tor/pt_state
TOR_PT_EXIT_ON_STDIN_CLOSE=1
TOR_PT_SERVER_TRANSPORTS=obfs3,obfs4
TOR_PT_SERVER_BINDADDR=obfs3-198.51.100.1:1984

Messages the PT Proxy writes to stdin:

VERSION 1
SMETHOD obfs3 198.51.100.1:1984
SMETHOD obfs4 198.51.100.1:43734 ARGS:cert=HszPy3vWfjsESCEOo9ZBkRv6zQ/1mGHzc8arF0y2SpwFr3WhsMu8rK0zyaoyERfbz3ddFw,iat-mode=0
SMETHODS DONE

