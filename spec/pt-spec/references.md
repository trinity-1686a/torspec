<a id="pt-spec.txt-5"></a>
# References

```text
   [RFC2119]     Bradner, S., "Key words for use in RFCs to Indicate
                 Requirement Levels", BCP 14, RFC 2119, March 1997.

   [RFC1928]     Leech, M., Ganis, M., Lee, Y., Kuris, R.,
                 Koblas, D., Jones, L., "SOCKS Protocol Version 5",
                 RFC 1928, March 1996.

   [EXTORPORT]   Kadianakis, G., Mathewson, N., "Extended ORPort and
                 TransportControlPort", Tor Proposal 196, March 2012.

   [RFC3986]     Berners-Lee, T., Fielding, R., Masinter, L., "Uniform
                 Resource Identifier (URI): Generic Syntax", RFC 3986,
                 January 2005.

   [RFC1929]     Leech, M., "Username/Password Authentication for
                 SOCKS V5", RFC 1929, March 1996.
```

