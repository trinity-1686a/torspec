<a id="pt-spec.txt-2"></a>
# Architecture Overview

```text
     +------------+                    +---------------------------+
     | Client App +-- Local Loopback --+ PT Client (SOCKS Proxy)   +--+
     +------------+                    +---------------------------+  |
                                                                      |
                 Public Internet (Obfuscated/Transformed traffic) ==> |
                                                                      |
     +------------+                    +---------------------------+  |
     | Server App +-- Local Loopback --+ PT Server (Reverse Proxy) +--+
     +------------+                    +---------------------------+
```

On the client's host, the PT Client software exposes a SOCKS proxy
[RFC1928] to the client application, and obfuscates or otherwise
transforms traffic before forwarding it to the server's host.

On the server's host, the PT Server software exposes a reverse proxy
that accepts connections from PT Clients, and handles reversing the
obfuscation/transformation applied to traffic, before forwarding it
to the actual server software.  An optional lightweight protocol
exists to facilitate communicating connection meta-data that would
otherwise be lost such as the source IP address and port
[EXTORPORT].

All PT instances are configured by the respective parent process via
a set of standardized environment variables (3.2) that are set at
launch time, and report status information back to the parent via
writing output in a standardized format to stdout (3.3).

Each invocation of a PT MUST be either a client OR a server.

All PT client forward proxies MUST support either SOCKS 4 or SOCKS 5,
and SHOULD prefer SOCKS 5 over SOCKS 4.

