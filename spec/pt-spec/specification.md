<a id="pt-spec.txt-3"></a>
# Specification

Pluggable Transport proxies follow the following workflow
throughout their lifespan.

```text
     1) Parent process sets the required environment values (3.2)
        and launches the PT proxy as a sub-process (fork()/exec()).

     2) The PT Proxy determines the versions of the PT specification
        supported by the parent"TOR_PT_MANAGED_TRANSPORT_VER" (3.2.1)

        2.1) If there are no compatible versions, the PT proxy
             writes a "VERSION-ERROR" message (3.3.1) to stdout and
             terminates.

        2.2) If there is a compatible version, the PT proxy writes
             a "VERSION" message (3.3.1) to stdout.

     3) The PT Proxy parses the rest of the environment values.

        3.1) If the environment values are malformed, or otherwise
             invalid, the PT proxy writes a "ENV-ERROR" message
             (3.3.1) to stdout and terminates.

        3.2) Determining if it is a client side forward proxy or
             a server side reverse proxy can be done via examining
             the "TOR_PT_CLIENT_TRANSPORTS" and "TOR_PT_SERVER_TRANSPORTS"
             environment variables.

     4) (Client only) If there is an upstream proxy specified via
        "TOR_PT_PROXY" (3.2.2), the PT proxy validates the URI
        provided.

        4.1) If the upstream proxy is unusable, the PT proxy writes
             a "PROXY-ERROR" message (3.3.2) to stdout and
             terminates.

        4.2) If there is a supported and well-formed upstream proxy
             the PT proxy writes a "PROXY DONE" message (3.3.2) to
             stdout.

     5) The PT Proxy initializes the transports and reports the
        status via stdout (3.3.2, 3.3.3)

     6) The PT Proxy forwards and transforms traffic as appropriate.

     7) Upon being signaled to terminate by the parent process (3.4),
        the PT Proxy gracefully shuts down.
```

