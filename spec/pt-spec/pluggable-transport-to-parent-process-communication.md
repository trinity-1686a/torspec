<a id="pt-spec.txt-3.3"></a>
## Pluggable Transport To Parent Process Communication

All Pluggable Transport Proxies communicate to the parent process
via writing NL-terminated lines to stdout.  The line metaformat is:

<Line> ::= <Keyword> <OptArgs> <NL>
<Keyword> ::= <KeywordChar> | <Keyword> <KeywordChar>
<KeywordChar> ::= <any US-ASCII alphanumeric, dash, and underscore>
<OptArgs> ::= <Args>*
<Args> ::= <SP> <ArgChar> | <Args> <ArgChar>
<ArgChar> ::= <any US-ASCII character but NUL or NL>
<SP> ::= <US-ASCII whitespace symbol (32)>
<NL> ::= <US-ASCII newline (line feed) character (10)>

The parent process MUST ignore lines received from PT proxies with
unknown keywords.

<a id="pt-spec.txt-3.3.1"></a>
### Common Messages

When a PT proxy first starts up, it must determine which version
of the Pluggable Transports Specification to use to configure
itself.

It does this via the "TOR_PT_MANAGED_TRANSPORT_VER" (3.2.1)
environment variable which contains all of the versions supported
by the application.

Upon determining the version to use, or lack thereof, the PT
proxy responds with one of two messages.

VERSION-ERROR <ErrorMessage>

The "VERSION-ERROR" message is used to signal that there was
no compatible Pluggable Transport Specification version
present in the "TOR_PT_MANAGED_TRANSPORT_VER" list.

The <ErrorMessage> SHOULD be set to "no-version" for
historical reasons but MAY be set to a useful error message
instead.

PT proxies MUST terminate after outputting a "VERSION-ERROR"
message.

Example:

VERSION-ERROR no-version

VERSION <ProtocolVersion>

The "VERSION" message is used to signal the Pluggable Transport
Specification version (as in "TOR_PT_MANAGED_TRANSPORT_VER")
that the PT proxy will use to configure its transports and
communicate with the parent process.

The version for the environment values and reply messages
specified by this document is "1".

PT proxies MUST either report an error and terminate, or output
a "VERSION" message before moving on to client/server proxy
initialization and configuration.

Example:

VERSION 1

After version negotiation has been completed the PT proxy must
then validate that all of the required environment variables are
provided, and that all of the configuration values supplied are
well formed.

At any point, if there is an error encountered related to
configuration supplied via the environment variables, it MAY
respond with an error message and terminate.

ENV-ERROR <ErrorMessage>

The "ENV-ERROR" message is used to signal the PT proxy's
failure to parse the configuration environment variables (3.2).

The <ErrorMessage> SHOULD consist of a useful error message
that can be used to diagnose and correct the root cause of
the failure.

PT proxies MUST terminate after outputting a "ENV-ERROR"
message.

Example:

ENV-ERROR No TOR_PT_AUTH_COOKIE_FILE when TOR_PT_EXTENDED_SERVER_PORT set

<a id="pt-spec.txt-3.3.2"></a>
### Pluggable Transport Client Messages

After negotiating the Pluggable Transport Specification version,
PT client proxies MUST first validate "TOR_PT_PROXY" (3.2.2) if
it is set, before initializing any transports.

Assuming that an upstream proxy is provided, PT client proxies
MUST respond with a message indicating that the proxy is valid,
supported, and will be used OR a failure message.

PROXY DONE

The "PROXY DONE" message is used to signal the PT proxy's
acceptance of the upstream proxy specified by "TOR_PT_PROXY".

PROXY-ERROR <ErrorMessage>

The "PROXY-ERROR" message is used to signal that the upstream
proxy is malformed/unsupported or otherwise unusable.

PT proxies MUST terminate immediately after outputting a
"PROXY-ERROR" message.

Example:

PROXY-ERROR SOCKS 4 upstream proxies unsupported.

After the upstream proxy (if any) is configured, PT clients then
iterate over the requested transports in "TOR_PT_CLIENT_TRANSPORTS"
and initialize the listeners.

For each transport initialized, the PT proxy reports the listener
status back to the parent via messages to stdout.

CMETHOD <transport> <'socks4','socks5'> <address:port>

The "CMETHOD" message is used to signal that a requested
PT transport has been launched, the protocol which the parent
should use to make outgoing connections, and the IP address
and port that the PT transport's forward proxy is listening on.

Example:

CMETHOD trebuchet socks5 127.0.0.1:19999

CMETHOD-ERROR <transport> <ErrorMessage>

The "CMETHOD-ERROR" message is used to signal that
requested PT transport was unable to be launched.

Example:

CMETHOD-ERROR trebuchet no rocks available

Once all PT transports have been initialized (or have failed), the
PT proxy MUST send a final message indicating that it has finished
initializing.

CMETHODS DONE

The "CMETHODS DONE" message signals that the PT proxy has
finished initializing all of the transports that it is capable
of handling.

Upon sending the "CMETHODS DONE" message, the PT proxy
initialization is complete.

Notes:

```text
    - Unknown transports in "TOR_PT_CLIENT_TRANSPORTS" are ignored
      entirely, and MUST NOT result in a "CMETHOD-ERROR" message.
      Thus it is entirely possible for a given PT proxy to
      immediately output "CMETHODS DONE".

    - Parent processes MUST handle "CMETHOD"/"CMETHOD-ERROR"
      messages in any order, regardless of ordering in
      "TOR_PT_CLIENT_TRANSPORTS".
```

<a id="pt-spec.txt-3.3.3"></a>
### Pluggable Transport Server Messages

PT server reverse proxies iterate over the requested transports
in "TOR_PT_CLIENT_TRANSPORTS" and initialize the listeners.

For each transport initialized, the PT proxy reports the listener
status back to the parent via messages to stdout.

SMETHOD <transport> <address:port> [options]

The "SMETHOD" message is used to signal that a requested
PT transport has been launched, the protocol which will be
used to handle incoming connections, and the IP address and
port that clients should use to reach the reverse-proxy.

If there is a specific <address:port> provided for a given
PT transport via "TOR_PT_SERVER_BINDADDR", the transport
MUST be initialized using that as the server address.

The OPTIONAL 'options' field is used to pass additional
per-transport information back to the parent process.

The currently recognized 'options' are:

ARGS:[<Key>=<Value>,]+[<Key>=<Value>]

```text
           The "ARGS" option is used to pass additional key/value
           formatted information that clients will require to use
           the reverse proxy.

           Equal signs and commas MUST be escaped with a backslash.

           Tor: The ARGS are included in the transport line of the
           Bridge's extra-info document.

       Examples:

         SMETHOD trebuchet 198.51.100.1:19999
         SMETHOD rot_by_N 198.51.100.1:2323 ARGS:N=13

     SMETHOD-ERROR <transport> <ErrorMessage>
```

The "SMETHOD-ERROR" message is used to signal that
requested PT transport reverse proxy was unable to be
launched.

Example:

SMETHOD-ERROR trebuchet no cows available

Once all PT transports have been initialized (or have failed), the
PT proxy MUST send a final message indicating that it has finished
initializing.

SMETHODS DONE

The "SMETHODS DONE" message signals that the PT proxy has
finished initializing all of the transports that it is capable
of handling.

Upon sending the "SMETHODS DONE" message, the PT proxy
initialization is complete.

<a id="pt-spec.txt-3.3.4"></a>
### Pluggable Transport Log Message

This message is for a client or server PT to be able to signal back to the
parent process via stdout or stderr any log messages.

A log message can be any kind of messages (human readable) that the PT
sends back so the parent process can gather information about what is going
on in the child process. It is not intended for the parent process to parse
and act accordingly but rather a message used for plain logging.

For example, the tor daemon logs those messages at the Severity level and
sends them onto the control port using the PT_LOG (see control-spec.txt)
event so any third party can pick them up for debugging.

The format of the message:

LOG SEVERITY=Severity MESSAGE=Message

The SEVERITY value indicate at which logging level the message applies.
The accepted values for <Severity> are: error, warning, notice, info, debug

The MESSAGE value is a human readable string formatted by the PT. The
<Message> contains the log message which can be a String or CString (see
section 2 in control-spec.txt).

Example:

LOG SEVERITY=debug MESSAGE="Connected to bridge A"

<a id="pt-spec.txt-3.3.5"></a>
### Pluggable Transport Status Message

This message is for a client or server PT to be able to signal back to the
parent process via stdout or stderr any status messages.

The format of the message:

STATUS TRANSPORT=Transport <K_1>=<V_1> [<K_2>=<V_2> ...]

The TRANSPORT value indicates a hint on what the PT is such has the name or
the protocol used for instance. As an example, obfs4proxy would use
"obfs4". Thus, the Transport value can be anything the PT itself defines
and it can be a String or CString (see section 2 in control-spec.txt).

The <K_n>=<V_n> values are specific to the PT and there has to be at least
one. They are messages that reflects the status that the PT wants to
report. <V_n> can be a String or CString.

Examples (fictional):

STATUS TRANSPORT=obfs4 ADDRESS=198.51.100.123:1234 CONNECT=Success
STATUS TRANSPORT=obfs4 ADDRESS=198.51.100.222:2222 CONNECT=Failed FINGERPRINT=<Fingerprint> ERRSTR="Connection refused"
STATUS TRANSPORT=trebuchet ADDRESS=198.51.100.15:443 PERCENT=42

