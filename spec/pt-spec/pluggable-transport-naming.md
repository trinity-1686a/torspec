<a id="pt-spec.txt-3.1"></a>
## Pluggable Transport Naming

Pluggable Transport names serve as unique identifiers, and every
PT MUST have a unique name.

PT names MUST be valid C identifiers.  PT names MUST begin with
a letter or underscore, and the remaining characters MUST be
ASCII letters, numbers or underscores.  No length limit is
imposted.

PT names MUST satisfy the regular expression "[a-zA-Z_][a-zA-Z0-9_]*".

