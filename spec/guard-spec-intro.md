Tor Guard Specification

```text
                           Isis Lovecruft
                         George Kadianakis
                              Ola Bini
                           Nick Mathewson

Table of Contents

    1. Introduction and motivation
    2. State instances
    3. Circuit Creation, Entry Guard Selection (1000 foot view)
        3.1 Path selection
            3.1.1 Managing entry guards
            3.1.2 Middle and exit node selection
        3.2 Circuit Building
    4. The algorithm.
        4.0. The guards listed in the current consensus. [Section:GUARDS]
        4.1. The Sampled Guard Set. [Section:SAMPLED]
        4.2. The Usable Sample [Section:FILTERED]
        4.3. The confirmed-guard list. [Section:CONFIRMED]
        4.4. The Primary guards [Section:PRIMARY]
        4.5. Retrying guards. [Section:RETRYING]
        4.6. Selecting guards for circuits. [Section:SELECTING]
        4.7. When a circuit fails. [Section:ON_FAIL]
        4.8. When a circuit succeeds [Section:ON_SUCCESS]
        4.9. Updating the list of waiting circuits [Section:UPDATE_WAITING]
        4.10. Whenever we get a new consensus. [Section:ON_CONSENSUS]
        4.11. Deciding whether to generate a new circuit.
        4.12. When we are missing descriptors.
    A. Appendices
        A.0. Acknowledgements
        A.1. Parameters with suggested values. [Section:PARAM_VALS]
        A.2. Random values [Section:RANDOM]
        A.3. Why not a sliding scale of primaryness? [Section:CVP]
        A.4. Controller changes
        A.5. Persistent state format
```

