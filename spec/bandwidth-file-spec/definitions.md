<a id="bandwidth-file-spec.txt-2.1"></a>
## Definitions

The following nonterminals are defined in Tor directory protocol
sections 1.2., 2.1.1., 2.1.3.:

```text
    bool
    Int
    SP (space)
    NL (newline)
    KeywordChar
    ArgumentChar
    nickname
    hexdigest (a '$', followed by 40 hexadecimal characters
      ([A-Fa-f0-9]))

  Nonterminal defined section 2 of version-spec.txt [4]:

    version_number

  We define the following nonterminals:

    Line ::= ArgumentChar* NL
    RelayLine ::= KeyValue (SP KeyValue)* NL
    HeaderLine ::= KeyValue NL
    KeyValue ::= Key "=" Value
    Key ::= (KeywordChar | "_")+
    Value ::= ArgumentCharValue+
    ArgumentCharValue ::= any printing ASCII character except NL and SP.
    Terminator ::= "=====" or "===="
                   Generators SHOULD use a 5-character terminator.
    Timestamp ::= Int
    Bandwidth ::= Int
    MasterKey ::= a base64-encoded Ed25519 public key, with
                  padding characters omitted.
    DateTime ::= "YYYY-MM-DDTHH:MM:SS", as in ISO 8601
    CountryCode ::= Two capital ASCII letters ([A-Z]{2}), as defined in
                    ISO 3166-1 alpha-2 plus "ZZ" to denote unknown country
                    (eg the destination is in a Content Delivery Network).
    CountryCodeList ::= One or more CountryCode(s) separated by a comma
                        ([A-Z]{2}(,[A-Z]{2})*).
```

Note that key_value and value are defined in Tor directory protocol
with different formats to KeyValue and Value here.

Tor versions earlier than 0.3.5.1-alpha require all lines in the file
to be 510 characters or less. The previous limit was 254 characters in
Tor 0.2.6.2-alpha and earlier. Parsers MAY ignore longer Lines.

Note that directory authorities are only supported on the two most
recent stable Tor versions, so we expect that line limits will be
removed after Tor 0.4.0 is released in 2019.

