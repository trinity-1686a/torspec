<a id="bandwidth-file-spec.txt-2"></a>
# Format details

```text
  The Bandwidth File MUST contain the following sections:
  - Header List (exactly once), which is a partially ordered list of
    - Header Lines (one or more times), then
  - Relay Lines (zero or more times), in an arbitrary order.
  If it does not contain these sections, parsers SHOULD ignore the file.
```

