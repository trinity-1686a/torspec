Tor Protocol Specification

```text
                              Roger Dingledine
                               Nick Mathewson

Table of Contents

    0. Preliminaries
        0.1. Notation and encoding
        0.2. Security parameters
        0.3. Ciphers
        0.4. A bad hybrid encryption algorithm, for legacy purposes
    1. System overview
        1.1. Keys and names
    2. Connections
        2.1. Picking TLS ciphersuites
        2.2. TLS security considerations
    3. Cell Packet format
    4. Negotiating and initializing connections
        4.1. Negotiating versions with VERSIONS cells
        4.2. CERTS cells
        4.3. AUTH_CHALLENGE cells
        4.4. AUTHENTICATE cells
            4.4.1. Link authentication type 1: RSA-SHA256-TLSSecret
            4.4.2. Link authentication type 3: Ed25519-SHA256-RFC5705
        4.5. NETINFO cells
    5. Circuit management
        5.1. CREATE and CREATED cells
            5.1.1. Choosing circuit IDs in create cells
            5.1.2. EXTEND and EXTENDED cells
            5.1.3. The "TAP" handshake
            5.1.4. The "ntor" handshake
            5.1.4.1. The "ntor-v3" handshake.
            5.1.5. CREATE_FAST/CREATED_FAST cells
            5.1.6. Additional data in CREATE/CREATED cells
        5.2. Setting circuit keys
            5.2.1. KDF-TOR
            5.2.2. KDF-RFC5869
        5.3. Creating circuits
            5.3.1. Canonical connections
        5.4. Tearing down circuits
        5.5. Routing relay cells
            5.5.1. Circuit ID Checks
            5.5.2. Forward Direction
                5.5.2.1. Routing from the Origin
                5.5.2.2. Relaying Forward at Onion Routers
            5.5.3. Backward Direction
                5.5.3.1. Relaying Backward at Onion Routers
            5.5.4. Routing to the Origin
        5.6. Handling relay_early cells
    6. Application connections and stream management
        6.1. Relay cells
            6.1.1. Calculating the 'Digest' field
        6.2. Opening streams and transferring data
            6.2.1. Opening a directory stream
        6.3. Closing streams
        6.4. Remote hostname lookup
    7. Flow control
        7.1. Link throttling
        7.2. Link padding
        7.3. Circuit-level flow control
            7.3.1. SENDME Cell Format
        7.4. Stream-level flow control
    8. Handling resource exhaustion
        8.1. Memory exhaustion
    9. Subprotocol versioning
        9.1. "Link"
        9.2. "LinkAuth"
        9.3. "Relay"
        9.4. "HSIntro"
        9.5. "HSRend"
        9.6. "HSDir"
        9.7. "DirCache"
        9.8. "Desc"
        9.9. "Microdesc"
        9.10. "Cons"
        9.11. "Padding"
        9.12. "FlowCtrl"
```

Note: This document aims to specify Tor as currently implemented, though it
may take it a little time to become fully up to date. Future versions of Tor
may implement improved protocols, and compatibility is not guaranteed.
We may or may not remove compatibility notes for other obsolete versions of
Tor as they become obsolete.

This specification is not a design document; most design criteria
are not examined.  For more information on why Tor acts as it does,
see tor-design.pdf.

