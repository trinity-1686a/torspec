<a id="srv-spec.txt-5"></a>
# Security Analysis

<a id="srv-spec.txt-5.1"></a>
## Security of commit-and-reveal and future directions

The security of commit-and-reveal protocols is well understood, and has
certain flaws. Basically, the protocol is insecure to the extent that an
adversary who controls b of the authorities gets to choose among 2^b
outcomes for the result of the protocol. However, an attacker who is not a
dirauth should not be able to influence the outcome at all.

We believe that this system offers sufficient security especially compared
to the current situation. More secure solutions require much more advanced
crypto and more complex protocols so this seems like an acceptable solution
for now.

Here are some examples of possible future directions:
- Schemes based on threshold signatures (e.g. see [HOPPER])
- Unicorn scheme by Lenstra et al. [UNICORN]
- Schemes based on Verifiable Delay Functions [VDFS]

For more alternative approaches on collaborative random number generation
also see the discussion at [RNGMESSAGING].

<a id="srv-spec.txt-5.2"></a>
## Predicting the shared random value during reveal phase

The reveal phase lasts 12 hours, and most authorities will send their
reveal value on the first round of the reveal phase. This means that an
attacker can predict the final shared random value about 12 hours before
it's generated.

This does not pose a problem for the HSDir hash ring, since we impose an
higher uptime restriction on HSDir nodes, so 12 hours predictability is not
an issue.

Any other protocols using the shared random value from this system should
be aware of this property.

<a id="srv-spec.txt-5.3"></a>
## Partition attacks

This design is not immune to certain partition attacks.  We believe they
don't offer much gain to an attacker as they are very easy to detect and
difficult to pull off since an attacker would need to compromise a directory
authority at the very least. Also, because of the byzantine general problem,
it's very hard (even impossible in some cases) to protect against all such
attacks. Nevertheless, this section describes all possible partition attack
and how to detect them.

<a id="srv-spec.txt-5.3.1"></a>
### Partition attacks during commit phase

A malicious directory authority could send only its commit to one single
authority which results in that authority having an extra commit value for
the shared random calculation that the others don't have. Since the
consensus needs majority, this won't affect the final SRV value. However,
the attacker, using this attack, could remove a single directory authority
from the consensus decision at 24:00 when the SRV is computed.

An attacker could also partition the authorities by sending two different
commitment values to different authorities during the commit phase.

All of the above is fairly easy to detect. Commitment values in the vote
coming from an authority should NEVER be different between authorities. If
so, this means an attack is ongoing or very bad bug (highly unlikely).

<a id="srv-spec.txt-5.3.2"></a>
### Partition attacks during reveal phase

Let's consider Alice, a malicious directory authority. Alice could wait
until the last reveal round, and reveal its value to half of the
authorities. That would partition the authorities into two sets: the ones
who think that the shared random value should contain this new reveal, and
the rest who don't know about it. This would result in a tie and two
different shared random value.

A similar attack is possible. For example, two rounds before the end of the
reveal phase, Alice could advertise her reveal value to only half of the
dirauths. This way, in the last reveal phase round, half of the dirauths
will include that reveal value in their votes and the others will not. In
the end of the reveal phase, half of the dirauths will calculate a
different shared randomness value than the others.

We claim that this attack is not particularly fruitful: Alice ends up
having two shared random values to choose from which is a fundamental
problem of commit-and-reveal protocols as well (since the last person can
always abort or reveal). The attacker can also sabotage the consensus, but
there are other ways this can be done with the current voting system.

Furthermore, we claim that such an attack is very noisy and detectable.
First of all, it requires the authority to sabotage two consensuses which
will cause quite some noise. Furthermore, the authority needs to send
different votes to different auths which is detectable. Like the commit
phase attack, the detection here is to make sure that the commitment values
in a vote coming from an authority are always the same for each authority.

