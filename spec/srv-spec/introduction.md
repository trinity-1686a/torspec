<a id="srv-spec.txt-1"></a>
# Introduction

<a id="srv-spec.txt-1.1"></a>
## Motivation

For the next generation hidden services project, we need the Tor network to
produce a fresh random value every day in such a way that it cannot be
predicted in advance or influenced by an attacker.

Currently we need this random value to make the HSDir hash ring
unpredictable (#8244), which should resolve a wide class of hidden service
DoS attacks and should make it harder for people to gauge the popularity
and activity of target hidden services. Furthermore this random value can
be used by other systems in need of fresh global randomness like
Tor-related protocols (e.g. OnioNS) or even non-Tor-related (e.g. warrant
canaries).

<a id="srv-spec.txt-1.2"></a>
## Previous work

Proposal 225 specifies a commit-and-reveal protocol that can be run as an
external script and have the results be fed to the directory authorities.
However, directory authority operators feel unsafe running a third-party
script that opens TCP ports and accepts connections from the Internet.
Hence, this proposal aims to embed the commit-and-reveal idea in the Tor
voting process which should make it smoother to deploy and maintain.

