<a id="srv-spec.txt-7"></a>
# Acknowledgements

Thanks to everyone who has contributed to this design with feedback and
discussion.

Thanks go to arma, ioerror, kernelcorn, nickm, s7r, Sebastian, teor, weasel
and everyone else!

References:

```text
[RANDOM-REFS]:
   http://projectbullrun.org/dual-ec/ext-rand.html
   https://lists.torproject.org/pipermail/tor-dev/2015-November/009954.html

[RNGMESSAGING]:
   https://moderncrypto.org/mail-archive/messaging/2015/002032.html

[HOPPER]:
   https://lists.torproject.org/pipermail/tor-dev/2014-January/006053.html

[UNICORN]:
   https://eprint.iacr.org/2015/366.pdf

[VDFS]:
   https://eprint.iacr.org/2018/601.pdf
```

