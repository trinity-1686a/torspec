Tor Path Specification

```text
                              Roger Dingledine
                               Nick Mathewson
```

Note: This is an attempt to specify Tor as currently implemented.  Future
versions of Tor will implement improved algorithms.

This document tries to cover how Tor chooses to build circuits and assign
streams to circuits.  Other implementations MAY take other approaches, but
implementors should be aware of the anonymity and load-balancing implications
of their choices.

THIS SPEC ISN'T DONE YET.

```text
      The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
      NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
      "OPTIONAL" in this document are to be interpreted as described in
      RFC 2119.

Tables of Contents

    1. General operation
        1.1. Terminology
        1.2. A relay's bandwidth
    2. Building circuits
        2.1. When we build
            2.1.0. We don't build circuits until we have enough directory info
            2.1.1. Clients build circuits preemptively
            2.1.2. Clients build circuits on demand
            2.1.3. Relays build circuits for testing reachability and bandwidth
            2.1.4. Hidden-service circuits
            2.1.5. Rate limiting of failed circuits
            2.1.6. When to tear down circuits
        2.2. Path selection and constraints
            2.2.1. Choosing an exit
            2.2.2. User configuration
        2.3. Cannibalizing circuits
        2.4. Learning when to give up ("timeout") on circuit construction
            2.4.1 Distribution choice and parameter estimation
            2.4.2. How much data to record
            2.4.3. How to record timeouts
            2.4.4. Detecting Changing Network Conditions
            2.4.5. Consensus parameters governing behavior
            2.4.6. Consensus parameters governing behavior
        2.5. Handling failure
    3. Attaching streams to circuits
    4. Hidden-service related circuits
    5. Guard nodes
        5.1. How consensus bandwidth weights factor into entry guard selection
    6. Server descriptor purposes
    7. Detecting route manipulation by Guard nodes (Path Bias)
        7.1. Measuring path construction success rates
        7.2. Measuring path usage success rates
        7.3. Scaling success counts
        7.4. Parametrization
        7.5. Known barriers to enforcement
    X. Old notes
    X.1. Do we actually do this?
    X.2. A thing we could do to deal with reachability.
    X.3. Some stuff that worries me about entry guards. 2006 Jun, Nickm.
```

