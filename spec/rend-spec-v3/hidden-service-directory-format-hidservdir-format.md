<a id="rend-spec-v3.txt-F"></a>
# Appendix F: Hidden service directory format [HIDSERVDIR-FORMAT]

This appendix section specifies the contents of the HiddenServiceDir directory:

- "hostname"                                       [FILE]

This file contains the onion address of the onion service.

- "private_key_ed25519"                            [FILE]

This file contains the private master ed25519 key of the onion service.
[TODO: Offline keys]

```text
  - "./authorized_clients/"                  [DIRECTORY]
    "./authorized_clients/alice.auth"        [FILE]
    "./authorized_clients/bob.auth"          [FILE]
    "./authorized_clients/charlie.auth"      [FILE]
```

If client authorization is enabled, this directory MUST contain a ".auth"
file for each authorized client. Each such file contains the public key of
the respective client. The files are transmitted to the service operator by
the client.

See section [CLIENT-AUTH-MGMT] for more details and the format of the client file.

(NOTE: client authorization is implemented as of 0.3.5.1-alpha.)

