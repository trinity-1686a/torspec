<a id="rend-spec-v3.txt-2.3"></a>
## Publishing shared random values [PUB-SHAREDRANDOM]

Our design for limiting the predictability of HSDir upload locations
relies on a shared random value (SRV) that isn't predictable in advance or
too influenceable by an attacker. The authorities must run a protocol
to generate such a value at least once per hsdir period. Here we
describe how they publish these values; the procedure they use to
generate them can change independently of the rest of this
specification. For more information see [SHAREDRANDOM-REFS].

According to proposal 250, we add two new lines in consensuses:

```text
     "shared-rand-previous-value" SP NUM_REVEALS SP VALUE NL
     "shared-rand-current-value" SP NUM_REVEALS SP VALUE NL
```

<a id="rend-spec-v3.txt-2.3.1"></a>
### Client behavior in the absence of shared random values

If the previous or current shared random value cannot be found in a
consensus, then Tor clients and services need to generate their own random
value for use when choosing HSDirs.

To do so, Tor clients and services use:

SRV = H("shared-random-disaster" | INT_8(period_length) | INT_8(period_num))

where period_length is the length of a time period in minutes,
rounded down; period_num is calculated as specified in
[TIME-PERIODS] for the wanted shared random value that could not be
found originally.

<a id="rend-spec-v3.txt-2.3.2"></a>
### Hidden services and changing shared random values

It's theoretically possible that the consensus shared random values will
change or disappear in the middle of a time period because of directory
authorities dropping offline or misbehaving.

To avoid client reachability issues in this rare event, hidden services
should use the new shared random values to find the new responsible HSDirs
and upload their descriptors there.

XXX How long should they upload descriptors there for?

