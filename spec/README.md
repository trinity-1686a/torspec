# Tor specifications

These specifications describe how Tor works.  They try to present
Tor's protocols in sufficient detail to allow the reader to implement
a compatible implementation of Tor without ever having to read the Tor
source code.

They were once a separate set of text files, but in late 2023 we
migrated to use [`mdbook`](https://rust-lang.github.io/mdBook/).
We're in the process of updating these documents to improve their quality.

This is a living document: we are always changing and improving them
in order to make them easier and more accurate, and to improve the
quality of the Tor protocols.  They are maintained as a set of
documents in a
[gitlab repository](https://gitlab.torproject.org/tpo/core/torspec/);
you can use that repository to see their history.

Additionally, the <a href="./proposals/index.html">proposals</a>
directory holds our design proposals.  These include historical
documents that have now been merged into the main specifications, and
new proposals that are still under discussion.  Of particular
interrest are the
<a href="./proposals/BY_STATUS.html#finished-proposals-implemented-specs-not-merged">`FINISHED`
Tor proposals</a>: They are the ones that have been implemented, but
not yet merged into these documents.

## Getting started

There's a lot of material here, and it's not always as well organized
as we like.  We have broken it into a few major sections.

For a table of contents, click on the menu icon to the top-left of
your browser scene.  You should probably start by reading [the core
tor protocol specification](./tor-spec-intro), which describes how our
protocol works.  After that, you should be able to jump around to
the topics that interest you most.  The introduction of each top-level
chapter should provide an introduction.

## How to edit these specifications

These specifications are displayed using a tool called
[`mdbook`](https://rust-lang.github.io/mdBook/); we recommend reading
its documentation.

To edit these specs, clone the [git repository] and edit the
appropriate file in the [`spec` directory].  These files will match
the URLs of their corresponding pages, so if you want to edit
[`tor-spec-intro.html`](./tor-spec-intro), you'll be looking for a file
called [`spec/tor-spec-intro.md`].

The chapter structure as a whole is defined in a special file called
[`SUMMARY.md`].  Its format is pretty restrictive; see
[the mdbook documentation](https://rust-lang.github.io/mdBook/format/summary.html)
for more information.

You can build the website locally to see how it renders.  To do this,
just install mdbook and run the `./build_html.sh` script.  (You will
need to have mdbook installed to do this.)

> TODO: Add a note about trying not to break links.

[git repository]: https://gitlab.torproject.org/tpo/core/torspec/
[`spec` directory]: https://gitlab.torproject.org/tpo/core/torspec/-/tree/main/spec?ref_type=heads
[`spec/tor-spec-intro.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/spec/tor-spec-intro.md?ref_type=heads
[`SUMMARY.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/raw/main/spec/SUMMARY.md?ref_type=heads


----

## Permalinks

Additionally, these URLs at `spec.toprorject.org` are intended to be
long-term permalinks.

> TODO: I'd like to revise these to point somewhere sensible again.
> The `gitweb.torproject.org` site is currently deprecated.

<dl>
<dt><a href="/address-spec"><code>/address-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/address-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/address-spec.txt</code> (Special Hostnames in Tor)</a></dt>
<dt><a href="/bandwidth-file-spec"><code>/bandwidth-file-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/bandwidth-file-spec.txt</code> (Directory Authority Bandwidth File spec)</a></dt>
<dt><a href="/bridgedb-spec"><code>/bridgedb-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/bridgedb-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/bridgedb-spec.txt</code> (BridgeDB specification)</a></dt>
<dt><a href="/cert-spec"><code>/cert-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/cert-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/cert-spec.txt</code> (Ed25519 certificates in Tor)</a></dt>
<dt><a href="/collector-protocol"><code>/collector-protocol</code></a></dt>
<dd><a href="https://gitweb.torproject.org/collector.git/tree/src/main/resources/docs/PROTOCOL"><code>https://gitweb.torproject.org/collector.git/tree/src/main/resources/docs/PROTOCOL</code> (Protocol of CollecTor's File Structure)</a></dt>
<dt><a href="/control-spec"><code>/control-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/control-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/control-spec.txt</code> (Tor control protocol, version 1)</a></dt>
<dt><a href="/dir-spec"><code>/dir-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/dir-spec.txt</code> (Tor directory protocol, version 3)</a></dt>
<dt><a href="/dir-list-spec"><code>/dir-list-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/dir-list-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/dir-list-spec.txt</code> (Tor Directory List file format)</a></dt>
<dt><a href="/ext-orport-spec"><code>/ext-orport-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/ext-orport-spec.txt</code> (Extended ORPort for pluggable transports)</a></dt>
<dt><a href="/gettor-spec"><code>/gettor-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/gettor-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/gettor-spec.txt</code> (GetTor specification)</a></dt>
<dt><a href="/padding-spec"><code>/padding-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/padding-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/padding-spec.txt</code> (Tor Padding Specification)</a></dt>
<dt><a href="/path-spec"><code>/path-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/path-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/path-spec.txt</code> (Tor Path Specification)</a></dt>
<dt><a href="/pt-spec"><code>/pt-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/pt-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/pt-spec.txt</code> (Tor Pluggable Transport Specification, version 1)</a></dt>
<dt><a href="/rend-spec"><code>/rend-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt"><code>https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt</code> (Tor Onion Service Rendezvous Specification, Version 2)</a></dt>
<dt><a href="/rend-spec-v2"><code>/rend-spec-v2</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/attic/rend-spec-v2.txt"><code>https://gitweb.torproject.org/torspec.git/tree/attic/rend-spec-v2.txt</code> (Tor Onion Service Rendezvous Specification, Version 2)</a></dt>
<dt><a href="/rend-spec-v3"><code>/rend-spec-v3</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt"><code>https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt</code> (Tor Onion Service Rendezvous Specification, Version 3)</a></dt>
<dt><a href="/socks-extensions"><code>/socks-extensions</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/socks-extensions.txt"><code>https://gitweb.torproject.org/torspec.git/tree/socks-extensions.txt</code> (Tor's extensions to the SOCKS protocol)</a></dt>
<dt><a href="/srv-spec"><code>/srv-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/srv-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/srv-spec.txt</code> (Tor Shared Random Subsystem Specification)</a></dt>
<dt><a href="/tor-fw-helper-spec"><code>/tor-fw-helper-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/attic/tor-fw-helper-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/attic/tor-fw-helper-spec.txt</code> (Tor's (little) Firewall Helper specification)</a></dt>
<dt><a href="/tor-spec"><code>/tor-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/tor-spec.txt</code> (Tor Protocol Specification)</a></dt>
<dt><a href="/torbrowser-design"><code>/torbrowser-design</code></a></dt>
<dd><a href="https://2019.www.torproject.org/projects/torbrowser/design/"><code>https://2019.www.torproject.org/projects/torbrowser/design/</code> (The Design and Implementation of the Tor Browser)</a></dt>
<dt><a href="/version-spec"><code>/version-spec</code></a></dt>
<dd><a href="https://gitweb.torproject.org/torspec.git/tree/version-spec.txt"><code>https://gitweb.torproject.org/torspec.git/tree/version-spec.txt</code> (How Tor Version Numbers Work)</a></dt>
<dt><a href="/tor-design"><code>/tor-design</code></a></dt>
<dd><a href="https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf"><code>https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf</code> (Tor: The Second-Generation Onion Router)</a></dt>
<dt><a href="/walking-onions"><code>/walking-onions</code></a></dt>
<dd><a href="https://github.com/nmathewson/walking-onions-wip/tree/master/specs"><code>https://github.com/nmathewson/walking-onions-wip/tree/master/specs</code> (Walking Onions specifications (work in progress))</a></dt>

</dl>
