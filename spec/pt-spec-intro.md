Pluggable Transport Specification (Version 1)

Abstract

Pluggable Transports (PTs) are a generic mechanism for the rapid
development and deployment of censorship circumvention,
based around the idea of modular sub-processes that transform
traffic to defeat censors.

This document specifies the sub-process startup, shutdown,
and inter-process communication mechanisms required to utilize
PTs.

Table of Contents

```text
   1. Introduction
      1.1. Requirements Notation
   2. Architecture Overview
   3. Specification
      3.1. Pluggable Transport Naming
      3.2. Pluggable Transport Configuration Environment Variables
           3.2.1. Common Environment Variables
           3.2.2. Pluggable Transport Client Environment Variables
           3.2.3. Pluggable Transport Server Environment Variables
      3.3. Pluggable Transport To Parent Process Communication
           3.3.1. Common Messages
           3.3.2. Pluggable Transport Client Messages
           3.3.3. Pluggable Transport Server Messages
      3.4. Pluggable Transport Shutdown
      3.5. Pluggable Transport Client Per-Connection Arguments
   4. Anonymity Considerations
   5 References
   6. Acknowledgments
   Appendix A. Example Client Pluggable Transport Session
   Appendix B. Example Server Pluggable Transport Session
```

