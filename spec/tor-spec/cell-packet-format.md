<a id="tor-spec.txt-3"></a>
# Cell Packet format

The basic unit of communication for onion routers and onion
proxies is a fixed-width "cell".

On a version 1 connection, each cell contains the following
fields:

```text
        CircID                                [CIRCID_LEN bytes]
        Command                               [1 byte]
        Payload (padded with padding bytes)   [PAYLOAD_LEN bytes]
```

On a version 2 or higher connection, all cells are as in version 1
connections, except for variable-length cells, whose format is:

```text
        CircID                                [CIRCID_LEN octets]
        Command                               [1 octet]
        Length                                [2 octets; big-endian integer]
        Payload (some commands MAY pad)       [Length bytes]
```

Most variable-length cells MAY be padded with padding bytes, except
for VERSIONS cells, which MUST NOT contain any additional bytes.
(The payload of VPADDING cells consists of padding bytes.)

On a version 2 connection, variable-length cells are indicated by a
command byte equal to 7 ("VERSIONS").  On a version 3 or
higher connection, variable-length cells are indicated by a command
byte equal to 7 ("VERSIONS"), or greater than or equal to 128.

CIRCID_LEN is 2 for link protocol versions 1, 2, and 3.  CIRCID_LEN
is 4 for link protocol version 4 or higher.  The first VERSIONS cell,
and any cells sent before the first VERSIONS cell, always have
CIRCID_LEN == 2 for backward compatibility.

The CircID field determines which circuit, if any, the cell is
associated with.

The 'Command' field of a fixed-length cell holds one of the following
values:

```text
         0 -- PADDING     (Padding)                 (See Sec 7.2)
         1 -- CREATE      (Create a circuit)        (See Sec 5.1)
         2 -- CREATED     (Acknowledge create)      (See Sec 5.1)
         3 -- RELAY       (End-to-end data)         (See Sec 5.5 and 6)
         4 -- DESTROY     (Stop using a circuit)    (See Sec 5.4)
         5 -- CREATE_FAST (Create a circuit, no KP) (See Sec 5.1)
         6 -- CREATED_FAST (Circuit created, no KP) (See Sec 5.1)
         8 -- NETINFO     (Time and address info)   (See Sec 4.5)
         9 -- RELAY_EARLY (End-to-end data; limited)(See Sec 5.6)
         10 -- CREATE2    (Extended CREATE cell)    (See Sec 5.1)
         11 -- CREATED2   (Extended CREATED cell)    (See Sec 5.1)
         12 -- PADDING_NEGOTIATE   (Padding negotiation)    (See Sec 7.2)

    Variable-length command values are:

         7 -- VERSIONS    (Negotiate proto version) (See Sec 4)
         128 -- VPADDING  (Variable-length padding) (See Sec 7.2)
         129 -- CERTS     (Certificates)            (See Sec 4.2)
         130 -- AUTH_CHALLENGE (Challenge value)    (See Sec 4.3)
         131 -- AUTHENTICATE (Client authentication)(See Sec 4.5)
         132 -- AUTHORIZE (Client authorization)    (Not yet used)

   The interpretation of 'Payload' depends on the type of the cell.

      VPADDING/PADDING:
               Payload contains padding bytes.
      CREATE/CREATE2:  Payload contains the handshake challenge.
      CREATED/CREATED2: Payload contains the handshake response.
      RELAY/RELAY_EARLY: Payload contains the relay header and relay body.
      DESTROY: Payload contains a reason for closing the circuit.
               (see 5.4)
```

Upon receiving any other value for the command field, an OR must
drop the cell.  Since more cell types may be added in the future, ORs
should generally not warn when encountering unrecognized commands.

The cell is padded up to the cell length with padding bytes.

Senders set padding bytes depending on the cell's command:

```text
      VERSIONS:  Payload MUST NOT contain padding bytes.
      AUTHORIZE: Payload is unspecified and reserved for future use.
      Other variable-length cells:
                 Payload MAY contain padding bytes at the end of the cell.
                 Padding bytes SHOULD be set to NUL.
      RELAY/RELAY_EARLY: Payload MUST be padded to PAYLOAD_LEN with padding
                  bytes. Padding bytes SHOULD be set to random values.
      Other fixed-length cells:
                 Payload MUST be padded to PAYLOAD_LEN with padding bytes.
                 Padding bytes SHOULD be set to NUL.
```

We recommend random padding in RELAY/RELAY_EARLY cells, so that the cell
content is unpredictable. See the format of relay cells in section 6.1
for detail.

For other cells, TLS authenticates cell content, so randomized padding
bytes are redundant.

Receivers MUST ignore padding bytes.

PADDING cells are currently used to implement connection keepalive.
If there is no other traffic, ORs and OPs send one another a PADDING
cell every few minutes.

CREATE, CREATE2, CREATED, CREATED2, and DESTROY cells are used to
manage circuits; see section 5 below.

RELAY cells are used to send commands and data along a circuit; see
section 6 below.

VERSIONS and NETINFO cells are used to set up connections in link
protocols v2 and higher; in link protocol v3 and higher, CERTS,
AUTH_CHALLENGE, and AUTHENTICATE may also be used.  See section 4
below.

