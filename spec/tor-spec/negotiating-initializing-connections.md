<a id="tor-spec.txt-4"></a>
# Negotiating and initializing connections

After Tor instances negotiate handshake with either the "renegotiation" or
"in-protocol" handshakes, they must exchange a set of cells to set up
the Tor connection and make it "open" and usable for circuits.

When the renegotiation handshake is used, both parties immediately
send a VERSIONS cell (4.1 below), and after negotiating a link
protocol version (which will be 2), each send a NETINFO cell (4.5
below) to confirm their addresses and timestamps.  No other intervening
cell types are allowed.

When the in-protocol handshake is used, the initiator sends a
VERSIONS cell to indicate that it will not be renegotiating.  The
responder sends a VERSIONS cell, a CERTS cell (4.2 below) to give the
initiator the certificates it needs to learn the responder's
identity, an AUTH_CHALLENGE cell (4.3) that the initiator must include
as part of its answer if it chooses to authenticate, and a NETINFO
cell (4.5).  As soon as it gets the CERTS cell, the initiator knows
whether the responder is correctly authenticated.  At this point the
initiator behaves differently depending on whether it wants to
authenticate or not. If it does not want to authenticate, it MUST
send a NETINFO cell.  If it does want to authenticate, it MUST send a
CERTS cell, an AUTHENTICATE cell (4.4), and a NETINFO.  When this
handshake is in use, the first cell must be VERSIONS, VPADDING, or
AUTHORIZE, and no other cell type is allowed to intervene besides
those specified, except for VPADDING cells.

The AUTHORIZE cell type is reserved for future use by scanning-resistance
designs.

[Tor versions before 0.2.3.11-alpha did not recognize the AUTHORIZE cell,
and did not permit any command other than VERSIONS as the first cell of
the in-protocol handshake.]

<a id="tor-spec.txt-4.1"></a>
## Negotiating versions with VERSIONS cells

There are multiple instances of the Tor link connection protocol.  Any
connection negotiated using the "certificates up front" handshake (see
section 2 above) is "version 1".  In any connection where both parties
have behaved as in the "renegotiation" handshake, the link protocol
version must be 2.  In any connection where both parties have behaved
as in the "in-protocol" handshake, the link protocol must be 3 or higher.

To determine the version, in any connection where the "renegotiation"
or "in-protocol" handshake was used (that is, where the responder
sent only one certificate at first and where the initiator did not
send any certificates in the first negotiation), both parties MUST
send a VERSIONS cell.  In "renegotiation", they send a VERSIONS cell
right after the renegotiation is finished, before any other cells are
sent.  In "in-protocol", the initiator sends a VERSIONS cell
immediately after the initial TLS handshake, and the responder
replies immediately with a VERSIONS cell. (As an exception to this rule,
if both sides support the "in-protocol" handshake, either side may send
VPADDING cells at any time.)

The payload in a VERSIONS cell is a series of big-endian two-byte
integers.  Both parties MUST select as the link protocol version the
highest number contained both in the VERSIONS cell they sent and in the
versions cell they received.  If they have no such version in common,
they cannot communicate and MUST close the connection.  Either party MUST
close the connection if the versions cell is not well-formed (for example,
if the payload contains an odd number of bytes).

Any VERSIONS cells sent after the first VERSIONS cell MUST be ignored.
(To be interpreted correctly, later VERSIONS cells MUST have a CIRCID_LEN
matching the version negotiated with the first VERSIONS cell.)

Since the version 1 link protocol does not use the "renegotiation"
handshake, implementations MUST NOT list version 1 in their VERSIONS
cell.  When the "renegotiation" handshake is used, implementations
MUST list only the version 2.  When the "in-protocol" handshake is
used, implementations MUST NOT list any version before 3, and SHOULD
list at least version 3.

Link protocols differences are:

```text
     1 -- The "certs up front" handshake.
     2 -- Uses the renegotiation-based handshake. Introduces
          variable-length cells.
     3 -- Uses the in-protocol handshake.
     4 -- Increases circuit ID width to 4 bytes.
     5 -- Adds support for link padding and negotiation (padding-spec.txt).
```

<a id="tor-spec.txt-4.2"></a>
## CERTS cells

The CERTS cell describes the keys that a Tor instance is claiming
to have.  It is a variable-length cell.  Its payload format is:

```text
        N: Number of certs in cell            [1 octet]
        N times:
           CertType                           [1 octet]
           CLEN                               [2 octets]
           Certificate                        [CLEN octets]

   Any extra octets at the end of a CERTS cell MUST be ignored.

     Relevant certType values are:
        1: Link key certificate certified by RSA1024 identity
        2: RSA1024 Identity certificate, self-signed.
        3: RSA1024 AUTHENTICATE cell link certificate, signed with RSA1024 key.
        4: Ed25519 signing key, signed with identity key.
        5: TLS link certificate, signed with ed25519 signing key.
        6: Ed25519 AUTHENTICATE cell key, signed with ed25519 signing key.
        7: Ed25519 identity, signed with RSA identity.
```

The certificate format for certificate types 1-3 is DER encoded
X509.  For others, the format is as documented in cert-spec.txt.
Note that type 7 uses a different format from types 4-6.

A CERTS cell may have no more than one certificate of each CertType.

To authenticate the responder as having a given Ed25519,RSA identity key
combination, the initiator MUST check the following.

```text
     * The CERTS cell contains exactly one CertType 2 "ID" certificate.
     * The CERTS cell contains exactly one CertType 4 Ed25519
       "Id->Signing" cert.
     * The CERTS cell contains exactly one CertType 5 Ed25519
       "Signing->link" certificate.
     * The CERTS cell contains exactly one CertType 7 "RSA->Ed25519"
       cross-certificate.
     * All X.509 certificates above have validAfter and validUntil dates;
       no X.509 or Ed25519 certificates are expired.
     * All certificates are correctly signed.
     * The certified key in the Signing->Link certificate matches the
       SHA256 digest of the certificate that was used to
       authenticate the TLS connection.
     * The identity key listed in the ID->Signing cert was used to
       sign the ID->Signing Cert.
     * The Signing->Link cert was signed with the Signing key listed
       in the ID->Signing cert.
     * The RSA->Ed25519 cross-certificate certifies the Ed25519
       identity, and is signed with the RSA identity listed in the
       "ID" certificate.
     * The certified key in the ID certificate is a 1024-bit RSA key.
     * The RSA ID certificate is correctly self-signed.
```

To authenticate the responder as having a given RSA identity only,
the initiator MUST check the following:

```text
     * The CERTS cell contains exactly one CertType 1 "Link" certificate.
     * The CERTS cell contains exactly one CertType 2 "ID" certificate.
     * Both certificates have validAfter and validUntil dates that
       are not expired.
     * The certified key in the Link certificate matches the
       link key that was used to negotiate the TLS connection.
     * The certified key in the ID certificate is a 1024-bit RSA key.
     * The certified key in the ID certificate was used to sign both
       certificates.
     * The link certificate is correctly signed with the key in the
       ID certificate
     * The ID certificate is correctly self-signed.
```

In both cases above, checking these conditions is sufficient to
authenticate that the initiator is talking to the Tor node with the
expected identity, as certified in the ID certificate(s).

To authenticate the initiator as having a given Ed25519,RSA
identity key combination, the responder MUST check the following:

```text
     * The CERTS cell contains exactly one CertType 2 "ID" certificate.
     * The CERTS cell contains exactly one CertType 4 Ed25519
       "Id->Signing" certificate.
     * The CERTS cell contains exactly one CertType 6 Ed25519
       "Signing->auth" certificate.
     * The CERTS cell contains exactly one CertType 7 "RSA->Ed25519"
       cross-certificate.
     * All X.509 certificates above have validAfter and validUntil dates;
       no X.509 or Ed25519 certificates are expired.
     * All certificates are correctly signed.
     * The identity key listed in the ID->Signing cert was used to
       sign the ID->Signing Cert.
     * The Signing->AUTH cert was signed with the Signing key listed
       in the ID->Signing cert.
     * The RSA->Ed25519 cross-certificate certifies the Ed25519
       identity, and is signed with the RSA identity listed in the
       "ID" certificate.
     * The certified key in the ID certificate is a 1024-bit RSA key.
     * The RSA ID certificate is correctly self-signed.
```

To authenticate the initiator as having an RSA identity key only,
the responder MUST check the following:

```text
     * The CERTS cell contains exactly one CertType 3 "AUTH" certificate.
     * The CERTS cell contains exactly one CertType 2 "ID" certificate.
     * Both certificates have validAfter and validUntil dates that
       are not expired.
     * The certified key in the AUTH certificate is a 1024-bit RSA key.
     * The certified key in the ID certificate is a 1024-bit RSA key.
     * The certified key in the ID certificate was used to sign both
       certificates.
     * The auth certificate is correctly signed with the key in the
       ID certificate.
     * The ID certificate is correctly self-signed.
```

Checking these conditions is NOT sufficient to authenticate that the
initiator has the ID it claims; to do so, the cells in 4.3 and 4.4
below must be exchanged.

<a id="tor-spec.txt-4.3"></a>
## AUTH_CHALLENGE cells

An AUTH_CHALLENGE cell is a variable-length cell with the following
fields:

```text
       Challenge [32 octets]
       N_Methods [2 octets]
       Methods   [2 * N_Methods octets]
```

It is sent from the responder to the initiator. Initiators MUST
ignore unexpected bytes at the end of the cell.  Responders MUST
generate every challenge independently using a strong RNG or PRNG.

The Challenge field is a randomly generated string that the
initiator must sign (a hash of) as part of authenticating.  The
methods are the authentication methods that the responder will
accept.  Only two authentication methods are defined right now:
see 4.4.1 and 4.4.2 below.

<a id="tor-spec.txt-4.4"></a>
## AUTHENTICATE cells

If an initiator wants to authenticate, it responds to the
AUTH_CHALLENGE cell with a CERTS cell and an AUTHENTICATE cell.
The CERTS cell is as a server would send, except that instead of
sending a CertType 1 (and possibly CertType 5) certs for arbitrary link
certificates, the initiator sends a CertType 3 (and possibly
CertType 6) cert for an RSA/Ed25519 AUTHENTICATE key.

This difference is because we allow any link key type on a TLS
link, but the protocol described here will only work for specific key
types as described in 4.4.1 and 4.4.2 below.

An AUTHENTICATE cell contains the following:

```text
        AuthType                              [2 octets]
        AuthLen                               [2 octets]
        Authentication                        [AuthLen octets]
```

Responders MUST ignore extra bytes at the end of an AUTHENTICATE
cell.  Recognized AuthTypes are 1 and 3, described in the next
two sections.

Initiators MUST NOT send an AUTHENTICATE cell before they have
verified the certificates presented in the responder's CERTS
cell, and authenticated the responder.

<a id="tor-spec.txt-4.4.1"></a>
### Link authentication type 1: RSA-SHA256-TLSSecret

If AuthType is 1 (meaning "RSA-SHA256-TLSSecret"), then the
Authentication field of the AUTHENTICATE cell contains the following:

```text
       TYPE: The characters "AUTH0001" [8 octets]
       CID: A SHA256 hash of the initiator's RSA1024 identity key [32 octets]
       SID: A SHA256 hash of the responder's RSA1024 identity key [32 octets]
       SLOG: A SHA256 hash of all bytes sent from the responder to the
         initiator as part of the negotiation up to and including the
         AUTH_CHALLENGE cell; that is, the VERSIONS cell, the CERTS cell,
         the AUTH_CHALLENGE cell, and any padding cells.  [32 octets]
       CLOG: A SHA256 hash of all bytes sent from the initiator to the
         responder as part of the negotiation so far; that is, the
         VERSIONS cell and the CERTS cell and any padding cells. [32
         octets]
       SCERT: A SHA256 hash of the responder's TLS link certificate. [32
         octets]
       TLSSECRETS: A SHA256 HMAC, using the TLS master secret as the
         secret key, of the following:
           - client_random, as sent in the TLS Client Hello
           - server_random, as sent in the TLS Server Hello
           - the NUL terminated ASCII string:
             "Tor V3 handshake TLS cross-certification"
          [32 octets]
       RAND: A 24 byte value, randomly chosen by the initiator.  (In an
         imitation of SSL3's gmt_unix_time field, older versions of Tor
         sent an 8-byte timestamp as the first 8 bytes of this field;
         new implementations should not do that.) [24 octets]
       SIG: A signature of a SHA256 hash of all the previous fields
         using the initiator's "Authenticate" key as presented.  (As
         always in Tor, we use OAEP-MGF1 padding; see tor-spec.txt
         section 0.3.)
          [variable length]
```

To check the AUTHENTICATE cell, a responder checks that all fields
from TYPE through TLSSECRETS contain their unique
correct values as described above, and then verifies the signature.
The server MUST ignore any extra bytes in the signed data after
the RAND field.

Responders MUST NOT accept this AuthType if the initiator has
claimed to have an Ed25519 identity.

(There is no AuthType 2: It was reserved but never implemented.)

<a id="tor-spec.txt-4.4.2"></a>
### Link authentication type 3: Ed25519-SHA256-RFC5705.

If AuthType is 3, meaning "Ed25519-SHA256-RFC5705", the
Authentication field of the AuthType cell is as below:

Modified values and new fields below are marked with asterisks.

```text
       TYPE: The characters "AUTH0003" [8 octets]
       CID: A SHA256 hash of the initiator's RSA1024 identity key [32 octets]
       SID: A SHA256 hash of the responder's RSA1024 identity key [32 octets]
       CID_ED: The initiator's Ed25519 identity key [32 octets]
       SID_ED: The responder's Ed25519 identity key, or all-zero. [32 octets]
       SLOG: A SHA256 hash of all bytes sent from the responder to the
         initiator as part of the negotiation up to and including the
         AUTH_CHALLENGE cell; that is, the VERSIONS cell, the CERTS cell,
         the AUTH_CHALLENGE cell, and any padding cells.  [32 octets]
       CLOG: A SHA256 hash of all bytes sent from the initiator to the
         responder as part of the negotiation so far; that is, the
         VERSIONS cell and the CERTS cell and any padding cells. [32
         octets]
       SCERT: A SHA256 hash of the responder's TLS link certificate. [32
         octets]
       TLSSECRETS: The output of an RFC5705 Exporter function on the
         TLS session, using as its inputs:
          - The label string "EXPORTER FOR TOR TLS CLIENT BINDING AUTH0003"
          - The context value equal to the initiator's Ed25519 identity key.
          - The length 32.
            [32 octets]
       RAND: A 24 byte value, randomly chosen by the initiator. [24 octets]
       SIG: A signature of all previous fields using the initiator's
          Ed25519 authentication key (as in the cert with CertType 6).
          [variable length]
```

To check the AUTHENTICATE cell, a responder checks that all fields
from TYPE through TLSSECRETS contain their unique
correct values as described above, and then verifies the signature.
The server MUST ignore any extra bytes in the signed data after
the RAND field.

<a id="tor-spec.txt-4.5"></a>
## NETINFO cells

If version 2 or higher is negotiated, each party sends the other a
NETINFO cell.  The cell's payload is:

```text
      TIME       (Timestamp)                     [4 bytes]
      OTHERADDR  (Other OR's address)            [variable]
         ATYPE   (Address type)                  [1 byte]
         ALEN    (Address length)                [1 byte]
         AVAL    (Address value in NBO)          [ALEN bytes]
      NMYADDR    (Number of this OR's addresses) [1 byte]
        NMYADDR times:
          ATYPE   (Address type)                 [1 byte]
          ALEN    (Address length)               [1 byte]
          AVAL    (Address value in NBO))        [ALEN bytes]

   Recognized address types (ATYPE) are:

     [04] IPv4.
     [06] IPv6.
```

ALEN MUST be 4 when ATYPE is 0x04 (IPv4) and 16 when ATYPE is 0x06
(IPv6).  If the ALEN value is wrong for the given ATYPE value, then
the provided address should be ignored.

The timestamp is a big-endian unsigned integer number of seconds
since the Unix epoch. Implementations MUST ignore unexpected bytes
at the end of the cell.  Clients SHOULD send "0" as their timestamp, to
avoid fingerprinting.

Implementations MAY use the timestamp value to help decide if their
clocks are skewed.  Initiators MAY use "other OR's address" to help
learn which address their connections may be originating from, if they do
not know it; and to learn whether the peer will treat the current
connection as canonical.  Implementations SHOULD NOT trust these
values unconditionally, especially when they come from non-authorities,
since the other party can lie about the time or IP addresses it sees.

Initiators SHOULD use "this OR's address" to make sure
that they have connected to another OR at its canonical address.
(See 5.3.1 below.)

