<a id="tor-spec.txt-1"></a>
# System overview

Tor is a distributed overlay network designed to anonymize
low-latency TCP-based applications such as web browsing, secure shell,
and instant messaging. Clients choose a path through the network and
build a ``circuit'', in which each node (or ``onion router'' or ``OR'')
in the path knows its predecessor and successor, but no other nodes in
the circuit.  Traffic flowing down the circuit is sent in fixed-size
``cells'', which are unwrapped by a symmetric key at each node (like
the layers of an onion) and relayed downstream.

<a id="tor-spec.txt-1.1"></a>
## Keys and names

Every Tor relay has multiple public/private keypairs:

These are 1024-bit RSA keys:

```text
    - A long-term signing-only "Identity key" used to sign documents and
      certificates, and used to establish relay identity.
      KP_relayid_rsa, KS_relayid_rsa.
    - A medium-term TAP "Onion key" used to decrypt onion skins when accepting
      circuit extend attempts.  (See 5.1.)  Old keys MUST be accepted for a
      while after they are no longer advertised.  Because of this,
      relays MUST retain old keys for a while after they're rotated.  (See
      "onion key lifetime parameters" in dir-spec.txt.)
      KP_onion_tap, KS_onion_tap.
    - A short-term "Connection key" used to negotiate TLS connections.
      Tor implementations MAY rotate this key as often as they like, and
      SHOULD rotate this key at least once a day.
      KP_conn_tls, KS_conn_tls.

   This is Curve25519 key:

    - A medium-term ntor "Onion key" used to handle onion key handshakes when
      accepting incoming circuit extend requests.  As with TAP onion keys,
      old ntor keys MUST be accepted for at least one week after they are no
      longer advertised.  Because of this, relays MUST retain old keys for a
      while after they're rotated. (See "onion key lifetime parameters" in
      dir-spec.txt.)
      KP_ntor, KS_ntor.

   These are Ed25519 keys:

    - A long-term "master identity" key.  This key never
      changes; it is used only to sign the "signing" key below.  It may be
      kept offline.
      KP_relayid_ed, KS_relayid_ed.
    - A medium-term "signing" key.  This key is signed by the master identity
      key, and must be kept online.  A new one should be generated
      periodically.  It signs nearly everything else.
      KP_relaysign_ed, KS_relaysign_ed.
    - A short-term "link authentication" key, used to authenticate
      the link handshake: see section 4 below.  This key is signed
      by the "signing" key, and should be regenerated frequently.
      KP_link_ed, KS_link_ed.
```

KP_relayid_* together identify a router uniquely.  Once a router
has used a KP_relayid_ed (an Ed25519 master identity key)
together with a given KP_relayid_rsa (RSA identity key), neither of
those keys may ever be used with a different key.

We write KP_relayid to refer to a key which is either
KP_relayid_rsa or KP_relayid_ed.

The same key or keypair should never be used for separate roles within
the Tor protocol suite, unless specifically stated.  For example,
a relay's identity keys K_relayid should not also be used as the
identity keypair for a hidden service K_hs_id (see rend-spec-v3.txt).

