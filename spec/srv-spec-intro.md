Tor Shared Random Subsystem Specification

This document specifies how the commit-and-reveal shared random subsystem of
Tor works. This text used to be proposal 250-commit-reveal-consensus.txt.

Table Of Contents:

```text
      1. Introduction
         1.1. Motivation
         1.2. Previous work
      2. Overview
         2.1. Introduction to our commit-and-reveal protocol
         2.2. Ten thousand feet view of the protocol
         2.3. How we use the consensus [CONS]
            2.3.1. Inserting Shared Random Values in the consensus
         2.4. Persistent State of the Protocol [STATE]
         2.5. Protocol Illustration
      3. Protocol
         3.1 Commitment Phase [COMMITMENTPHASE]
            3.1.1. Voting During Commitment Phase
            3.1.2. Persistent State During Commitment Phase [STATECOMMIT]
         3.2 Reveal Phase
            3.2.1. Voting During Reveal Phase
            3.2.2. Persistent State During Reveal Phase [STATEREVEAL]
         3.3. Shared Random Value Calculation At 00:00UTC
            3.3.1. Shared Randomness Calculation [SRCALC]
         3.4. Bootstrapping Procedure
         3.5. Rebooting Directory Authorities [REBOOT]
      4. Specification [SPEC]
         4.1. Voting
            4.1.1. Computing commitments and reveals [COMMITREVEAL]
            4.1.2. Validating commitments and reveals [VALIDATEVALUES]
            4.1.4. Encoding commit/reveal values in votes [COMMITVOTE]
            4.1.5. Shared Random Value [SRVOTE]
         4.2. Encoding Shared Random Values in the consensus [SRCONSENSUS]
         4.3. Persistent state format [STATEFORMAT]
      5. Security Analysis
         5.1. Security of commit-and-reveal and future directions
         5.2. Predicting the shared random value during reveal phase
         5.3. Partition attacks
            5.3.1. Partition attacks during commit phase
            5.3.2. Partition attacks during reveal phase
      6. Discussion
         6.1. Why the added complexity from proposal 225?
         6.2. Why do you do a commit-and-reveal protocol in 24 rounds?
         6.3. Why can't we recover if the 00:00UTC consensus fails?
      7. Acknowledgements
```

